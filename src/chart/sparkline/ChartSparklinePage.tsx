import React, { useState, useEffect } from 'react';
import SubHeader, { SubHeaderLeft } from './../../layout/SubHeader/SubHeader';
import Breadcrumb from './../../components/bootstrap/Breadcrumb';
import Page from './../../layout/Page/Page';
import PageWrapper from './../../layout/PageWrapper/PageWrapper';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from './../../components/bootstrap/Card';
import Chart, { IChartOptions } from './../../components/extras/Chart';
import { FormDataHelper } from '../../services/FormDataHelper';
import { GeneralServices } from '../../services/General.Services';
import { TOP_STORES_BY_ORDERS } from '../../services/axiousURL';

const ChartSparklinePage = () => {
	const [topOrders, setTopOrders] = useState<any[]>([]);
	useEffect(() => {
		let helper = FormDataHelper();
		GeneralServices.postRequest(helper, TOP_STORES_BY_ORDERS).then(
			(resp) => {
				if (resp.data.length > 5) {
					setTopOrders(resp.data.slice(0, 5));
				} else {
					setTopOrders(resp.data);
				}
			},
			(err) => {},
		);
	}, []);
console.log({topOrders})
	return (
		<PageWrapper>
			<Page>
				<Card>
					<CardHeader><CardLabel icon='Countertops' iconColor='primary'>
						<CardTitle>Top 5 Pharmacies By Orders</CardTitle>
					</CardLabel></CardHeader>
					
					<CardBody>
						<div className='row'>
							<div className='col-12'>
								<div className='table-responsive'>
									<table className='table table-modern'>
										<thead>
											<tr>
												<th>Store Name</th>
												<th>Pharmacy Name</th>
												<th>Store Address</th>
												<th>Total Orders</th>
												<th>Total Price</th>
											</tr>
										</thead>
										<tbody>
											{topOrders?.map((order, index) => {
												return (
													<>
														<tr key={index}>
															<td>{order?.store_name}</td>
															<td>{order?.pharmacy_name}</td>
															<td>{order?.store_address}</td>
															<td>{order?.orders_count}</td>
															<td>{order?.total_order_price}</td>
														</tr>
													</>
												);
											})}
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default ChartSparklinePage;
