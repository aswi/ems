import BeveledCone from '../../assets/img/abstract/beveled-cone.png';

const data: {
	id: number;
	image: string;
	name: string;
	registrationNumber: string;
	phoneNumber: string;
	storeAddress: string;
	storeLat: number;
	storeLong: number;
	store: string;
	file: string;
}[] = [
	{
		id: 1,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 2,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 3,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 4,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 5,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 6,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 7,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 8,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 9,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 10,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
	{
		id: 11,
		image: BeveledCone,
		name: 'Beveled Cone',
		registrationNumber: '2321ADs@1212312',
		phoneNumber: '+92333-455',
		storeAddress: 'store address',
		storeLat: 380,
		storeLong: 14.5,
		store: 'Company A',
		file: 'Figma',
	},
];
export default data;
