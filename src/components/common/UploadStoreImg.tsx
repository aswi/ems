import React, { useState } from 'react';
import Avatar from '../../components/Avatar';
import Input from '../../components/bootstrap/forms/Input';
import Button from '../../components/bootstrap/Button';
import USERS from '../../common/data/userDummyData';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../components/bootstrap/Card';

interface ImageUploaderProps {
	defaultImage: string;
	onImageSelected: (file: File | null) => void;
	allowDelete?: boolean;
	onDeleteImage?: () => void;
}

const ImageUploader: React.FC<ImageUploaderProps> = ({
	defaultImage,
	onImageSelected,
	allowDelete = false,
	onDeleteImage,
}) => {
	const [preview, setPreview] = useState<string | undefined>(undefined);

	const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		const file = event.target.files?.[0] || null;

		onImageSelected(file);
		setPreview(file ? URL.createObjectURL(file) : undefined);
	};
	return (
		<Card>
			<CardHeader>
				<CardLabel icon='FormatSize' iconColor='warning'>
					<CardTitle>Employee</CardTitle>
					<CardSubTitle>Upload Employee Profile Picture</CardSubTitle>
				</CardLabel>
			</CardHeader>
			<CardBody>
				<div className='row g-4 align-items-center'>
					<div className='col-lg-auto'>
						<Avatar
							src={preview || defaultImage }
							color={USERS.JOHN.color}
							rounded={3}
						/>
					</div>
					<div className='col-lg'>
						<div className='row g-4'>
							<div className='col-auto'>
								<Input
									type='file'
									id='storeImage'
									name='storeImage'
									autoComplete='photo'
									accept='image/png, image/jpeg'
									onChange={handleFileChange}
								/>
							</div>
							{allowDelete && (
								<div className='col-auto'>
									<Button
										color='dark'
										isLight
										icon='Delete'
										onClick={onDeleteImage}>
										Delete Avatar
									</Button>
								</div>
							)}
							<div className='col-12'>
								<p className='lead text-muted'>
								Employee Profile Picture helps others get to know you.
								</p>
							</div>
						</div>
					</div>
				</div>
			</CardBody>
		</Card>
	);
};
export default ImageUploader;
