const regexs = {
	nameRegex: {
		regex:  /^[A-Za-z\s\-.,'&]{3,}$/,
		errMessage: 'Please enter a valid name.',
	},
	emailRegex: {
		regex: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/,
		errMessage: 'Please enter a valid email address.',
	},
	phoneRegex: {
		//   regex: /^\d{10}$/,
		regex: /^\+?[0-9]{1,4}[-.()\s]?([0-9]{1,3})?[-.()\s]?[0-9]{1,4}[-.()\s]?[0-9]{9,}$/,
		errMessage: 'Please enter a valid phone number.',
	},
	usernameRegex: {
		regex: /^[a-zA-Z0-9_]{3,16}$/,
		errMessage:
			'Username must be between 3 and 16 characters and can only contain letters, numbers, and underscores.',
	},
	titleRegex: {
		regex: /^[a-zA-Z\s]{1,100}$/,
		errMessage:
			'Title must be between 1 and 100 characters and can only contain letters and spaces.',
	},
	registrationNumberRegex: {
		regex: /^[A-Z]{3}\d{3}(-\d{3})?$/,
		errMessage:
			"Please enter a valid pharmacy registration number in the format 'AAA111' or 'AAA111-111'.",
	},
	passwordRegex: {
		regex: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
		errMessage:
			'Password must be at least 8 characters long and contain at least one letter and one number.',
	},
	confirmPasswordRegex: {
		regex: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
		errMessage: 'Passwords must match and meet the password requirements.',
	},
	addressRegex: {
		regex: /^[a-zA-Z0-9\s,'-.#]+$/,
		errMessage:
			'Please enter a valid address. It can only contain letters, numbers, spaces, commas, apostrophes, hyphens, periods, and hash symbols.',
	},
	pharmacyNameRegex: {
		regex: /^[a-zA-Z]{5,}$/,
		errMessage: 'Pharmacy name must contain at least five characters.',
	},
};

export default regexs;
