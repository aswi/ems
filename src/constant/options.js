export const LEAVE_OPTIONS = [
    { value: 'Casual Leave', text: 'Casual Leave' },
    { value: 'Sick leave', text: 'Sick leave' },
    { value: 'Annual leave', text: 'Annual leave' },
    // { value: 'Public Holiday', text: 'Public Holiday' },
    { value: 'Remote Work', text: 'Remote Work' },
];
export const TIME_OPTIONS = [
    { value: 'Half Time', text: 'Half Time' },
    { value: 'Full Time', text: 'Full Time' },
];
export const DEPARTMENT_OPTIONS = [
    { value: 'Mobile App Developers', text: 'Mobile App Developers' },
    { value: 'Front-end Developers', text: 'Front-end Developers' },
    { value: 'Front-end Developers', text: 'Front-end Developers' },

    { value: 'Back-end Developers', text: 'Back-end Developers' },

    { value: 'Full-stack Developers', text: 'Full-stack Developers' },

    { value: 'Front-end Developers', text: 'Front-end Developers' },

];

export const PROJECT_PRIORITY = [
    { value: 'Low', text: 'Low' },
    { value: 'Medium', text: 'Medium' },
    { value: 'High', text: 'High' }
];


