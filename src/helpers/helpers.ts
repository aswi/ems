export function test() {
	return null;
}

export function getOS() {
	const { userAgent } = window.navigator;
	const { platform } = window.navigator;
	const macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'];
	const windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'];
	const iosPlatforms = ['iPhone', 'iPad', 'iPod'];
	let os = null;

	if (macosPlatforms.indexOf(platform) !== -1) {
		os = 'MacOS';
	} else if (iosPlatforms.indexOf(platform) !== -1) {
		os = 'iOS';
	} else if (windowsPlatforms.indexOf(platform) !== -1) {
		os = 'Windows';
	} else if (/Android/.test(userAgent)) {
		os = 'Android';
	} else if (!os && /Linux/.test(platform)) {
		os = 'Linux';
	}

	// @ts-ignore
	document.documentElement.setAttribute('os', os);
	return os;
}

export const hasNotch = () => {
	/**
	 * For storybook test
	 */
	const storybook = window.location !== window.parent.location;
	// @ts-ignore
	const iPhone = /iPhone/.test(navigator.userAgent) && !window.MSStream;
	const aspect = window.screen.width / window.screen.height;
	const aspectFrame = window.innerWidth / window.innerHeight;
	return (
		(iPhone && aspect.toFixed(3) === '0.462') ||
		(storybook && aspectFrame.toFixed(3) === '0.462')
	);
};

export const mergeRefs = (refs: any[]) => {
	return (value: any) => {
		refs.forEach((ref) => {
			if (typeof ref === 'function') {
				ref(value);
			} else if (ref != null) {
				ref.current = value;
			}
		});
	};
};

export const randomColor = () => {
	const colors = ['primary', 'secondary', 'success', 'info', 'warning', 'danger'];

	const color = Math.floor(Math.random() * colors.length);

	return colors[color];
};

export const priceFormat = (price: number) => {
	return price.toLocaleString('en-US', {
		style: 'currency',
		currency: 'USD',
	});
};

export const average = (array: any[]) => array.reduce((a, b) => a + b) / array.length;

export const percent = (value1: number, value2: number) =>
	Number(((value1 / value2 - 1) * 100).toFixed(2));

export const getFirstLetter = (text: string, letterCount = 2): string =>
	// @ts-ignore
	text
		.toUpperCase()
		.match(/\b(\w)/g)
		.join('')
		.substring(0, letterCount);

export const debounce = (func: (arg0: any) => void, wait = 1000) => {
	let timeout: string | number | NodeJS.Timeout | undefined;

	return function executedFunction(...args: any[]) {
		const later = () => {
			clearTimeout(timeout);
			// @ts-ignore
			func(...args);
		};

		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
	};
};

export const capitalizeFirst = (str: string) => {
	return str.charAt(0).toUpperCase() + str.slice(1);
};


export const downloadImage = (imageUrl: string, imageName: string): void => {
	fetch(imageUrl, {
		method: 'GET',
		headers: {},
	})
		.then((response) => {
			response.arrayBuffer().then(function (buffer) {
				const url = window.URL.createObjectURL(new Blob([buffer]));
				const link = document.createElement('a');
				link.href = url;
				link.setAttribute('download', imageName); //or any other extension
				document.body.appendChild(link);
				link.click();
			});
		})
		.catch((err) => {
			console.log(err);
		});
};


// export function FormatDate(inputDate:any) {
// 	const months = [
// 	  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
// 	];
  
// 	const dateParts = inputDate.split(' ')[0].split('-');
// 	const formattedDate = `${months[parseInt(dateParts[1], 10) - 1]}-${dateParts[2]}-${dateParts[0]} ${inputDate.split(' ')[1]}`;
  
// 	const inputDateTime = new Date(inputDate).getTime();
// 	const currentTime = new Date().getTime();
// 	const timeDifference = currentTime - inputDateTime;
// 	const daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
  
// 	let distance = '';
// 	if (daysDifference === 0) {
// 	  const hoursDifference = Math.floor(timeDifference / (1000 * 60 * 60));
// 	  if (hoursDifference === 0) {
// 		const minutesDifference = Math.floor(timeDifference / (1000 * 60));
// 		if (minutesDifference === 0) {
// 		  distance = 'just now';
// 		} else {
// 		  distance = `${minutesDifference} ${minutesDifference === 1 ? 'minute' : 'minutes'} ago`;
// 		}
// 	  } else {
// 		const timeString = new Date(inputDateTime).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: false });
// 		distance = `${hoursDifference} ${hoursDifference === 1 ? 'hour' : 'hours'} ago (${timeString})`;
// 	  }
// 	} else if (daysDifference === 1) {
// 	  distance = `yesterday (${new Date(inputDateTime).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: false })})`;
// 	} else if (daysDifference > 1 && daysDifference <= 30) {
// 	  distance = `${daysDifference} ${daysDifference === 1 ? 'day' : 'days'} ago`;
// 	} else if (daysDifference > 30 && daysDifference <= 60) {
// 	  distance = 'a month ago';
// 	} else if (daysDifference > 60 && daysDifference <= 365) {
// 	  const monthsAgo = Math.floor(daysDifference / 30);
// 	  distance = `${monthsAgo} ${monthsAgo === 1 ? 'month' : 'months'} ago`;
// 	} else {
// 	  const yearsAgo = Math.floor(daysDifference / 365);
// 	  distance = `${yearsAgo} ${yearsAgo === 1 ? 'year' : 'years'} ago`;
// 	}
  
// 	return { formattedDate, distance };
//   }
  
//   // Example usage:
//   const inputDateFromDatabase = '2023-7-26 19:05:12';
//   const { formattedDate, distance } = FormatDate(inputDateFromDatabase);
//   console.log(formattedDate); // Output: "Jul-21-2023 02:05:12"
//   console.log(distance); // Output: "in 2 years"
  
  export function FormatDate(inputDate:any) {
	const months = [
	  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
	];
  
	const dateParts = inputDate.split(' ')[0].split('-');
	const formattedDate = `${months[parseInt(dateParts[1], 10) - 1]}-${dateParts[2]}-${dateParts[0]} ${inputDate.split(' ')[1]}`;
  
	const inputDateTime = new Date(inputDate).getTime();
	const currentTime = new Date().getTime();
	const timeDifference = currentTime - inputDateTime;
	const daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
  
	let distance = '';
	if (daysDifference === 0) {
	  distance = 'today';
	} else if (daysDifference === 1) {
	  distance = 'yesterday';
	} else if (daysDifference > 1 && daysDifference <= 30) {
	  distance = `${daysDifference} days ago`;
	} else if (daysDifference > 30 && daysDifference <= 60) {
	  distance = 'a month ago';
	} else if (daysDifference > 60 && daysDifference <= 365) {
	  const monthsAgo = Math.floor(daysDifference / 30);
	  distance = `${monthsAgo} months ago`;
	} else {
	  const yearsAgo = Math.floor(daysDifference / 365);
	  distance = `${yearsAgo} years ago`;
	}
  
	return { formattedDate, distance };
  }