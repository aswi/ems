import React, { useState } from 'react';
import Alert, { AlertHeading } from '../components/bootstrap/Alert';
import { TColor } from '../type/color-type';
export const useAlert = () => {
	const [isVisible, setVisible] = useState(false);
	const [messageState, setMessageState] = useState<string>('');
	const [variantState, setVariantState] = useState<TColor>('warning');

	const showMessage = () => setVisible(true);
	const hideMessage = () => {
		setVisible(false);
	};

	const getProps = ({ variant, message }: { variant: any; message: any }) => {
		setVariantState(variant);
		setMessageState(message);
		showMessage();
	};

	const AlertMessage: React.FC = () => {
		// Specify the type as React.FC
		return (
			<Alert onClose={hideMessage} color={variantState} isDismissible>
				<AlertHeading>{process.env.REACT_APP_SITE_NAME}</AlertHeading>
				<p className='text-capitalize'>{messageState}</p>
			</Alert>
		);
	};

	return [AlertMessage, isVisible, getProps] as const;
};
