import React, { lazy, useContext, useEffect, useState } from 'react';
import { Route, Routes } from 'react-router-dom';
// import contents from '../../routes/contentRoutes' AdminRoutes, SuperAdminRoutes,;
import {  auth, SuperAdminRoutes } from '../../routes/contentRoutes';
import AuthContext from '../../contexts/authContext';

const PAGE_404 = lazy(() => import('../../pages/presentation/auth/Page404'));
interface RouteData {
	path: string;
	element: React.ReactNode;
}
const ContentRoutes = () => {
	const { userData } = useContext(AuthContext);
	const [contents, setContent] = useState<RouteData[] | null>(null);

	useEffect(() => {
		// setIsSuperAdmin(userData && userData.is_super_admin === 'Y');
		// if (userData.is_super_admin === 'Y') {
		// 	setContent([...SuperAdminRoutes]);
		// } else if (userData.is_super_admin === 'N') {
		// 	setContent([...AdminRoutes]);
		// } else
		//  {
			setContent([...auth]);
		// }
	}, [userData.is_super_admin]);
	if (contents === null) {
		return null;
	}

	return (
		<Routes>
			{contents?.map((page) => (
				// eslint-disable-next-line react/jsx-props-no-spreading
				<Route key={page.path} {...page} />
			))}
			<Route path='*' element={<PAGE_404 />} />
		</Routes>
	);
};

export default ContentRoutes;
