import React, { FC, useContext } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import Checks from '../../components/bootstrap/forms/Checks';
import Chart from '../../components/extras/Chart';
import Badge from '../../components/bootstrap/Badge';
import Button from '../../components/bootstrap/Button';
import { demoPagesMenu, pageLayoutTypesPagesMenu } from '../../menu';
import useDarkMode from '../../hooks/useDarkMode';
import { ApexOptions } from 'apexcharts';
import Swal from 'sweetalert2';
import { FormDataHelper } from '../../services/FormDataHelper';
import { GeneralServices } from '../../services/General.Services';
import { UPDATE_CATEGORY_STATUS } from '../../services/axiousURL';
import AuthContext from '../../contexts/authContext';

interface ICategoryTableRowProps {
	id: number;
	title: string;
	description: string;
	status: string;
	removeCategory: any;
}

const ApproveCategoryTableRow: FC<ICategoryTableRowProps> = ({
	id,
	title,
	description,
	status,
	removeCategory,
}) => {
	if (status == 'Y') status = 'approved';
	else if (status == 'N') status = 'rejected';
	else status = 'pending';

	const { userData } = useContext(AuthContext);

	const updateStatus = (categoryStatus: string, category_id: number) => {
		Swal.fire({
			title: 'MedSouk',
			text: 'Are you sure you want to update the status?',
			showCancelButton: true,
			confirmButtonText: 'Save',
		}).then((result) => {
			if (result.isConfirmed) {
				let helper = FormDataHelper();
				helper.append('category_id', category_id.toString());
				helper.append('status', categoryStatus);
				helper.append('admin_id', userData.admin_id!);
				GeneralServices.postRequest(helper, UPDATE_CATEGORY_STATUS).then(
					(resp) => {
						Swal.fire(resp.message, '', 'success');
						removeCategory(category_id.toString());
					},
					(err) => {
						Swal.fire(err.message, '', 'error');
					},
				);
			}
		});
	};

	return (
		<tr>
			<th scope='row'>{id}</th>
			<td>
				<div>{title}</div>
			</td>
			<td>{description}</td>
			<td className='h5'>
				<Badge
					color={
						(status === 'rejected' && 'danger') ||
						(status === 'pending' && 'warning') ||
						(status === 'approved' && 'success') ||
						'info'
					}>
					{` ${status} `}
				</Badge>
			</td>
			<td className='text-end'>
				{status == 'pending' && (
					<div className='d-flex flex-row justify-content-center'>
						<div>
							<Button
								color='success'
								onClick={() => {
									updateStatus('Y', id);
								}}>
								Approve
							</Button>
						</div>
						<div className='ps-2'>
							<Button
								color='danger'
								onClick={() => {
									updateStatus('N', id);
								}}>
								Reject
							</Button>
						</div>
					</div>
				)}
			</td>
		</tr>
	);
};

export default ApproveCategoryTableRow;
