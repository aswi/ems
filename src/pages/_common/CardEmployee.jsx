import React, { useState, useEffect } from 'react';
import Card, {
	CardActions,
	
	CardHeader,
	CardLabel,
	
	CardTitle,
} from '../../components/bootstrap/Card';

import { superAdminPageLayoutTypesPagesMenu } from '../../menu';
import { useNavigate } from 'react-router-dom';
import { FormDataHelper } from './../../services/FormDataHelper';
import { GeneralServices } from './../../services/General.Services';
import { LIST_CATEGORIES } from './../../services/axiousURL';
import { LIST_PHARMACIES } from './../../services/axiousURL';
import { LIST_CUSTOMERS } from './../../services/axiousURL';

const CardEmpDashboard= () => {


	const navigate = useNavigate();
	
	// useEffect(() => {
	// 	let helper = FormDataHelper();
	// 	GeneralServices.postRequest(helper, LIST_CATEGORIES).then(
	// 		async (resp) => {
	// 			let cats = await resp.data;

	// 			const pending = await cats.filter((single) => single.status == 'P');

	// 			setApproveCategories(pending?.length);
	// 		},
	// 		(err) => {
	// 			console.log(err);
	// 		},
	// 	);
	// }, []);
	// useEffect(() => {
	// 	let helper = FormDataHelper();
	// 	GeneralServices.postRequest(helper, LIST_PHARMACIES).then(
	// 		(resp) => {
	// 			let pharms = resp.data;
	// 			pharms = pharms.filter((single) => single.status == 'Y');
	// 			let pending = resp?.data?.filter((single) => single.status == 'P');
	// 			setApprovedPharmacies(pharms.length);
	// 			setPendingPharmacies(pending.length);
	// 		},
	// 		(err) => {
	// 			console.log(err);
	// 		},
	// 	);
	// }, []);
	// useEffect(() => {
	// 	let helper = FormDataHelper();

	// 	GeneralServices.postRequest(helper, LIST_CUSTOMERS).then(
	// 		(resp) => {
	// 			setCustomers(resp.data.length);

	// 			let orderlenth = resp?.data?.reduce((accumulator, item) => {
	// 				return accumulator + parseInt(item.total_orders);
	// 			}, 0);
	// 			setTotalOrders(orderlenth);
	// 		},
	// 		(err) => {
	// 			console.log(err);
	// 		},
	// 	);
	// }, []);
	// let totalOrders=0

	return (
		<div className='row'>
			<div className='col-md-3 '>
				<Card stretch>
					<CardHeader borderSize={3} borderColor='success'>
						<CardLabel icon='Animation' iconColor='success'>
							<CardTitle>Project Assigned</CardTitle>
							
						</CardLabel>
						<CardActions>
						<div className='fw-bold fs-2 m-4 text-success'>3</div>
						</CardActions>
					</CardHeader>
					
				</Card>
			</div>
			<div className='col-md-3 '>
				<Card stretch>
					<CardHeader borderSize={3} borderColor='primary'>
						<CardLabel icon='WheelchairPickup' iconColor='primary'>
							<CardTitle>Today</CardTitle>
							
						</CardLabel>
						<CardActions>
							{/* <Button
								icon='ArrowForwardIos'
								aria-label='Read More'
								hoverShadow='default'
								onClick={() =>
									navigate(
										superAdminPageLayoutTypesPagesMenu.pharmacies.subMenu
											.listPendingPharmacies.path,
									)
								}
								// color={darkModeStatus ? 'dark' : undefined}
								// onClick={handleOnClickToEmployeeListPage}
							/> */}
								<div className='fw-bold fs-6 m-4 mb-5 text-primary'>In Office</div>
						</CardActions>
					</CardHeader>
				
				</Card>
			</div>
			<div className='col-md-3 '>
				<Card stretch>
					<CardHeader borderSize={3} borderColor='warning'>
						<CardLabel icon='LocalFireDepartment' iconColor='warning'>
							<CardTitle>Attandance</CardTitle>
							
						</CardLabel>
						<CardActions>
							{/* <Button
								icon='ArrowForwardIos'
								aria-label='Read More'
								hoverShadow='default'
								onClick={() =>
									navigate(
										superAdminPageLayoutTypesPagesMenu.categories.subMenu
											.approveRequest.path,
									)
								}
								// color={darkModeStatus ? 'dark' : undefined}
								// onClick={handleOnClickToEmployeeListPage}
							/> */}
								<div className='fw-bold fs-2 m-4 text-warning'>90%</div>
						</CardActions>
					</CardHeader>
					
				</Card>
			</div>
			<div className='col-md-3 '>
				<Card stretch>
					<CardHeader borderSize={3} borderColor='secondary'>
						<CardLabel icon='BeachAccess' iconColor='secondary'>
							<CardTitle>Available Leaves</CardTitle>
							{/* <CardSubTitle className='text-muted'>
								<strong>4</strong>
								
							</CardSubTitle> */}
						</CardLabel>
						<CardActions>
							{/* <Button
								icon='ArrowForwardIos'
								aria-label='Read More'
								hoverShadow='default'
								onClick={() =>
									navigate(superAdminPageLayoutTypesPagesMenu.customers.path)
								}
								// color={darkModeStatus ? 'dark' : undefined}
								// onClick={handleOnClickToEmployeeListPage}
							/> */}
								<div className='fw-bold fs-2 m-4 text-secondary'>18</div>
						</CardActions>
					</CardHeader>
					
				</Card>
			</div>
		</div>
	);
};

export default CardEmpDashboard;
