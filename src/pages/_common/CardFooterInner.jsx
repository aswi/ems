import React, { useState, useEffect } from 'react';
import Card, {
	CardActions,
	
	CardHeader,
	CardLabel,
	
	CardTitle,
} from '../../components/bootstrap/Card';

import { superAdminPageLayoutTypesPagesMenu } from '../../menu';
import { useNavigate } from 'react-router-dom';
import { FormDataHelper } from './../../services/FormDataHelper';
import { GeneralServices } from './../../services/General.Services';
import { LIST_CATEGORIES } from './../../services/axiousURL';
import { LIST_PHARMACIES } from './../../services/axiousURL';
import { LIST_CUSTOMERS } from './../../services/axiousURL';

const CardFooterInner = () => {
	// const [approvecategories, setApproveCategories] = useState(0);
	// const [approvedpharmacies, setApprovedPharmacies] = useState(0);
	// const [pendingpharmacies, setPendingPharmacies] = useState(0);
	// const [customers, setCustomers] = useState(0);
	// const [totalOrders, setTotalOrders] = useState([]);

	// const navigate = useNavigate();
	// const handleNavigate = () => {
	// 	navigate(superAdminPageLayoutTypesPagesMenu.pharmacies.subMenu.listApprovedPharmacies.path);
	// };
	// useEffect(() => {
	// 	let helper = FormDataHelper();
	// 	GeneralServices.postRequest(helper, LIST_CATEGORIES).then(
	// 		async (resp) => {
	// 			let cats = await resp.data;

	// 			const pending = await cats.filter((single) => single.status == 'P');

	// 			setApproveCategories(pending?.length);
	// 		},
	// 		(err) => {
	// 			console.log(err);
	// 		},
	// 	);
	// }, []);
	// useEffect(() => {
	// 	let helper = FormDataHelper();
	// 	GeneralServices.postRequest(helper, LIST_PHARMACIES).then(
	// 		(resp) => {
	// 			let pharms = resp.data;
	// 			pharms = pharms.filter((single) => single.status == 'Y');
	// 			let pending = resp?.data?.filter((single) => single.status == 'P');
	// 			setApprovedPharmacies(pharms.length);
	// 			setPendingPharmacies(pending.length);
	// 		},
	// 		(err) => {
	// 			console.log(err);
	// 		},
	// 	);
	// }, []);
	// useEffect(() => {
	// 	let helper = FormDataHelper();

	// 	GeneralServices.postRequest(helper, LIST_CUSTOMERS).then(
	// 		(resp) => {
	// 			setCustomers(resp.data.length);

	// 			let orderlenth = resp?.data?.reduce((accumulator, item) => {
	// 				return accumulator + parseInt(item.total_orders);
	// 			}, 0);
	// 			setTotalOrders(orderlenth);
	// 		},
	// 		(err) => {
	// 			console.log(err);
	// 		},
	// 	);
	// }, []);
	// let totalOrders=0

	return (
		<div className='row'>
			<div className='col-md-3 '>
				<Card stretch>
					<CardHeader borderSize={3} borderColor='primary'>
						<CardLabel icon='Bloodtype' iconColor='success'>
							<CardTitle>New Projects</CardTitle>
							
						</CardLabel>
						<CardActions>
						<div className='fw-bold fs-5 m-4 text-success'>8</div>
						</CardActions>
					</CardHeader>
					
				</Card>
			</div>
			<div className='col-md-3 '>
				<Card stretch>
					<CardHeader borderSize={3} borderColor='warning'>
						<CardLabel icon='Bloodtype' iconColor='warning'>
							<CardTitle>Employees</CardTitle>
							
						</CardLabel>
						<CardActions>
							{/* <Button
								icon='ArrowForwardIos'
								aria-label='Read More'
								hoverShadow='default'
								onClick={() =>
									navigate(
										superAdminPageLayoutTypesPagesMenu.pharmacies.subMenu
											.listPendingPharmacies.path,
									)
								}
								// color={darkModeStatus ? 'dark' : undefined}
								// onClick={handleOnClickToEmployeeListPage}
							/> */}
								<div className='fw-bold fs-5 m-4 text-success'>8</div>
						</CardActions>
					</CardHeader>
				
				</Card>
			</div>
			<div className='col-md-3 '>
				<Card stretch>
					<CardHeader borderSize={3} borderColor='primary'>
						<CardLabel icon='Category' iconColor='primary'>
							<CardTitle>Departments</CardTitle>
							
						</CardLabel>
						<CardActions>
							{/* <Button
								icon='ArrowForwardIos'
								aria-label='Read More'
								hoverShadow='default'
								onClick={() =>
									navigate(
										superAdminPageLayoutTypesPagesMenu.categories.subMenu
											.approveRequest.path,
									)
								}
								// color={darkModeStatus ? 'dark' : undefined}
								// onClick={handleOnClickToEmployeeListPage}
							/> */}
								<div className='fw-bold fs-5 m-4 text-success'>8</div>
						</CardActions>
					</CardHeader>
					
				</Card>
			</div>
			<div className='col-md-3 '>
				<Card stretch>
					<CardHeader borderSize={3} borderColor='secondary'>
						<CardLabel icon='PersonPinCircle' iconColor='secondary'>
							<CardTitle>Number On Leaves</CardTitle>
							{/* <CardSubTitle className='text-muted'>
								<strong>4</strong>
								
							</CardSubTitle> */}
						</CardLabel>
						<CardActions>
							{/* <Button
								icon='ArrowForwardIos'
								aria-label='Read More'
								hoverShadow='default'
								onClick={() =>
									navigate(superAdminPageLayoutTypesPagesMenu.customers.path)
								}
								// color={darkModeStatus ? 'dark' : undefined}
								// onClick={handleOnClickToEmployeeListPage}
							/> */}
								<div className='fw-bold fs-5 m-4 text-success'>8</div>
						</CardActions>
					</CardHeader>
					
				</Card>
			</div>
		</div>
	);
};

export default CardFooterInner;
