import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import Checks from '../../components/bootstrap/forms/Checks';
import Chart from '../../components/extras/Chart';
import Badge from '../../components/bootstrap/Badge';
import Button from '../../components/bootstrap/Button';
import { demoPagesMenu, pageLayoutTypesPagesMenu } from '../../menu';
import useDarkMode from '../../hooks/useDarkMode';
import { ApexOptions } from 'apexcharts';

interface ICategoryTableRowProps {
	id: number;
	title: string;
	description: string;
	status: string;
}
const CategoryTableRow: FC<ICategoryTableRowProps> = ({ id, title, description, status }) => {
	const { darkModeStatus } = useDarkMode();

	return (
		<tr>
			<th scope='row'>{id}</th>
			<td>
				<div>{title}</div>
			</td>
			<td>{description}</td>
			<td className='h5'>
				<Badge
					color={
						(status === 'R' && 'danger') ||
						(status === 'P' && 'warning') ||
						(status === 'Y' && 'success') ||
						'info'
					}>
					{status == 'Y' ? 'Approved' : 'Pending'}
				</Badge>
			</td>
			<td className='text-end'>
				{status == 'P' ? (
					<Button
						color='dark'
						isLight
						icon='Edit'
						tag='a'
						to={`../${pageLayoutTypesPagesMenu.projects.subMenu.editProjects.path}/${id}`}
					/>
				) : (
					''
				)}
			</td>
		</tr>
	);
};

export default CategoryTableRow;
