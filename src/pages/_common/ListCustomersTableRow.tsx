import React, { FC } from 'react';
import { FormatDate } from '../../helpers/helpers';

interface ICustomerTableRowProps {
	first_name: string;
	last_name: string;
	phone_number: string;
	address: string;
	total_orders?: string;
	created_datetime: string;
}

const ListCustomersTableRow: FC<ICustomerTableRowProps> = ({
	first_name,
	last_name,
	phone_number,
	address,
	total_orders,
	created_datetime,
}) => {
	const { formattedDate, distance } = FormatDate(created_datetime);
	
	return (
		<tr>
			<th>{first_name + ' ' + last_name}</th>
			<td>{phone_number}</td>
			<td>{address}</td>
			<td>{total_orders}</td>
			<td><td>
				<div>{formattedDate}</div>
				<div>
					<small className='text-muted'>{distance}</small>
				</div>
			</td></td>
		</tr>
	);
};

export default ListCustomersTableRow;
