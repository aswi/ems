import React from 'react';
import { HashLoader } from 'react-spinners';

interface DataErrorComponentProps {
	error: string[];
	loading: boolean;
	length?: number;
}

const DataErrorComponent: React.FC<DataErrorComponentProps> = ({ error, loading, length }) => {
	if (loading) {
		return (
			<div className='hash-loader-container'>
				<HashLoader color='#36d7b7' />
			</div>
		);
	} else if (length === 0) {
		return (
			<div className='DataErrorComponent'>
				{error.length > 0 ? (
					<>
						{error} 
						{/* Please Check your internet connection: Ensure that you have a stable
						internet connection and try refreshing the page. */}
					</>
				) : (
					<>No data found</>
				)}
			</div>
		);
	} else {
		return null;
	}
};

export default DataErrorComponent;
