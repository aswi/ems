import React from 'react';
import { FC } from 'react';
import Button from '../../components/bootstrap/Button';
import { pageLayoutTypesPagesMenu } from '../../menu';
import { useNavigate } from 'react-router-dom';
import { FormatDate, downloadImage } from '../../helpers/helpers';

interface IOrderTableRowProps {
	id: number;
	customer_first_name: string;
	customer_last_name: string;
	customer_phone_number: string;
	product_quantity: string;
	created_datetime: string;
	order_total_price: string;
	customer_email_address: string;
	prescription_image: string;
	store_name: string;
	item: object;
}

const OrderTableRow: FC<IOrderTableRowProps> = ({
	id: order_id,
	customer_first_name: first_name,
	customer_last_name: last_name,
	customer_phone_number: order_customer_phone_number,
	product_quantity: order_product_quantity,
	created_datetime: order_date,
	order_total_price: order_product_total_price,
	customer_email_address: order_customer_address,
	prescription_image: order_prescription_image,
	store_name,
	item,
}) => {
	const navigate = useNavigate();
	console.log(item)
	const handleNavigateOrderDetail = () => {
		navigate(pageLayoutTypesPagesMenu.attendance.subMenu.attendanceDetail.path, { state: item });
	};
	
	// function formatDate(inputDate: any) {
	// 	const months = [
	// 		'Jan',
	// 		'Feb',
	// 		'Mar',
	// 		'Apr',
	// 		'May',
	// 		'Jun',
	// 		'Jul',
	// 		'Aug',
	// 		'Sep',
	// 		'Oct',
	// 		'Nov',
	// 		'Dec',
	// 	];

	// 	const dateParts = inputDate.split(' ')[0].split('-');
	// 	const formattedDate = `${months[parseInt(dateParts[1], 10) - 1]}-${dateParts[2]}-${
	// 		dateParts[0]
	// 	} ${inputDate.split(' ')[1]}`;

	// 	const inputDateTime = new Date(inputDate).getTime();
	// 	const currentTime = new Date().getTime();
	// 	const timeDifference = currentTime - inputDateTime;
	// 	const daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));

	// 	let distance = '';
	// 	if (daysDifference === 0) {
	// 		const hoursDifference = Math.floor(timeDifference / (1000 * 60 * 60));
	// 		if (hoursDifference === 0) {
	// 			distance = 'just now';
	// 		} else {
	// 			distance = `${hoursDifference} ${hoursDifference === 1 ? 'hour' : 'hours'} ago`;
	// 		}
	// 	} else if (daysDifference === 1) {
	// 		distance = 'yesterday';
	// 	} else if (daysDifference > 1 && daysDifference <= 30) {
	// 		distance = `${daysDifference} ${daysDifference === 1 ? 'day' : 'days'} ago`;
	// 	} else if (daysDifference > 30 && daysDifference <= 60) {
	// 		distance = 'a month ago';
	// 	} else if (daysDifference > 60 && daysDifference <= 365) {
	// 		const monthsAgo = Math.floor(daysDifference / 30);
	// 		distance = `${monthsAgo} ${monthsAgo === 1 ? 'month' : 'months'} ago`;
	// 	} else {
	// 		const yearsAgo = Math.floor(daysDifference / 365);
	// 		distance = `${yearsAgo} ${yearsAgo === 1 ? 'year' : 'years'} ago`;
	// 	}

	// 	return { formattedDate, distance };
	// }

	 const { formattedDate, distance } = FormatDate(order_date);
	
	

	return (
		<tr>
			<th scope='row'>{order_id}</th>
			<td>
				<div>{first_name + ' ' + last_name}</div>
			</td>
			<td>{order_customer_phone_number}</td>
			<td>{store_name}</td>
			<td>{order_product_quantity}</td>
			<td>
				<div>{formattedDate}</div>
				<div>
					<small className='text-muted'>{distance}</small>
				</div>
			</td>
			<td>{order_product_total_price}</td>
			{/* <td>{order_customer_address}</td> */}
			<td className='text-center'>
				{(order_prescription_image != '' || order_prescription_image != null) && (
					<Button
						color='dark'
						isLight
						icon='download'
						onClick={() =>
							downloadImage(
								order_prescription_image,
								`${first_name}_${last_name}_${order_id}.png`,
							)
						}
					/>
				)}
			</td>
			<td className='text-end'>
				<Button
					color='dark'
					isLight
					icon='Visibility'
					tag='a'
					// to={`../${pageLayoutTypesPagesMenu.orders.subMenu.orderDetail.path}/${order_id}`}
					onClick={handleNavigateOrderDetail}
				/>
			</td>
		</tr>
	);
};

export default OrderTableRow;
