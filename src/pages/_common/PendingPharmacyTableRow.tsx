import React, { FC, useContext } from 'react';
import Badge from '../../components/bootstrap/Badge';
import Button from '../../components/bootstrap/Button';
import Swal from 'sweetalert2';
import { FormDataHelper } from '../../services/FormDataHelper';
import { GeneralServices } from '../../services/General.Services';
import { UPDATE_CATEGORY_STATUS, UPDATE_PHARMACY_STATUS } from '../../services/axiousURL';
import AuthContext from '../../contexts/authContext';

interface ICategoryTableRowProps {
	id: number;
	title: string;
	type: string;
	admin_username: string;
	admin_email_address: string;
	admin_phone_number: string;
	status: string;
	removePharmacy: any;
}

const PendingPharmacyTableRow: FC<ICategoryTableRowProps> = ({
	id,
	title,
	type,
	admin_username,
	admin_email_address,
	admin_phone_number,
	status,
	removePharmacy,
}) => {
	if (status == 'Y') status = 'approved';
	else if (status == 'N') status = 'rejected';
	else status = 'pending';

	const { userData } = useContext(AuthContext);

	const updateStatus = (pharmacyStatus: string, pharmacyId: number) => {
		Swal.fire({
			title: 'MedSouk',
			text: 'Are you sure you want to update the status?',
			showCancelButton: true,
			confirmButtonText: 'Save',
		}).then((result) => {
			if (result.isConfirmed) {
				let helper = FormDataHelper();
				helper.append('pharmacy_id', pharmacyId.toString());
				helper.append('status', pharmacyStatus);
				helper.append('admin_id', userData.admin_id!);
				GeneralServices.postRequest(helper, UPDATE_PHARMACY_STATUS).then(
					(resp) => {
						Swal.fire(resp.message, '', 'success');
						removePharmacy(pharmacyId.toString());
					},
					(err) => {
						Swal.fire(err.message, '', 'error');
					},
				);
			}
		});
	};

	return (
		<tr>
			<th scope='row'>{id}</th>
			<td>
				<div>{title}</div>
			</td>
			<td>{type}</td>
			<td>{admin_username}</td>
			<td>{admin_phone_number}</td>
			<td>{admin_email_address}</td>
			<td className='h5'>
				<Badge
					color={
						(status === 'rejected' && 'danger') ||
						(status === 'pending' && 'warning') ||
						(status === 'approved' && 'success') ||
						'info'
					}>
					{` ${status} `}
				</Badge>
			</td>
			<td className='text-end'>
				{status == 'pending' && (
					<div className='d-flex flex-row justify-content-center'>
						<div>
							<Button
								color='success'
								onClick={() => {
									updateStatus('Y', id);
								}}>
								Approve
							</Button>
						</div>
						{/* <div className='ps-2'>
							<Button
								color='danger'
								onClick={() => {
									updateStatus('N', id);
								}}>
								Reject
							</Button>
						</div> */}
					</div>
				)}
			</td>
		</tr>
	);
};

export default PendingPharmacyTableRow;
