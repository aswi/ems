import React, { FC } from 'react';
import { pageLayoutTypesPagesMenu } from '../../menu';
import Button from '../../components/bootstrap/Button';
// import productImg from '../../assets/img/animation-items/item.png';

interface IProductTableRowProps {
	id: number;
	images_data: any[];
	title: string;
	price: string;
	short_description: string;
	product_category: string;
}
const ProductTableRow: FC<IProductTableRowProps> = ({
	id,
	images_data,
	title: name,
	price: product_price,
	short_description: product_short_description,
	product_category,
}) => {
	return (
		<tr>
			<th scope='row'>{id}</th>
			<th>
				<img src={images_data[0].image_path} alt={name} width={50} />
			</th>
			<td>
				<div>{name}</div>
			</td>
			<td>{product_price}</td>
			<td>{product_category}</td>
			<td>{product_short_description}</td>
			<td className='text-center'>
				{/* <Button color='dark' isLight icon='Delete' /> */}
				<Button
					color='dark'
					isLight
					icon='Edit'
					tag='a'
					className='ms-3'
					to={`../${pageLayoutTypesPagesMenu.leaves.subMenu.editleaves.path}/${id}`}
				/>
			</td>
		</tr>
	);
};

export default ProductTableRow;
