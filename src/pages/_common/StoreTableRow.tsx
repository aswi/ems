import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import Checks from '../../components/bootstrap/forms/Checks';
import Chart from '../../components/extras/Chart';
import Badge from '../../components/bootstrap/Badge';
import Button from '../../components/bootstrap/Button';
import { demoPagesMenu, pageLayoutTypesPagesMenu } from '../../menu';
import useDarkMode from '../../hooks/useDarkMode';
import { ApexOptions } from 'apexcharts';
import img from '../../assets/img/animation-items/item2.png';
import webimg from '../../assets/img/animation-items/item2.webp';

interface IStoreTableRowProps {
	id: number;
	image: string;
	name: string;
	registration_number: string;
	phone_number: string;
	address: string;
	latitude: number;
	longitude: number;
	store: string;
	file: string;
}
const StoreTableRow: FC<IStoreTableRowProps> = ({
	id,
	image,
	name,
	registration_number,
	phone_number: phoneNumber,
	address: storeAddress,
	latitude: storeLat,
	longitude: storeLong,
	store,
	file,
}) => {
	const { darkModeStatus } = useDarkMode();

	return (
		<tr>
			<th scope='row'>{id}</th>
			<td>
				<img src={image} alt={name} width={54} height={54} />
			</td>
			<td>
				<div>{name}</div>
			</td>
			<td>{registration_number}</td>
			<td>{phoneNumber}</td>
			<td>{storeAddress}</td>
			<td>{storeLat}</td>
			<td>{storeLong}</td>
			<td className='text-end'>
				<Button
					color='dark'
					isLight
					icon='Edit'
					tag='a'
					to={`../${pageLayoutTypesPagesMenu.employee.subMenu.editEmployee.path}/${id}`}
				/>
			</td>
		</tr>
	);
};

export default StoreTableRow;
