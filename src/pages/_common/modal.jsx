import React, {useState} from 'react'
import Modal, {
	ModalBody,
	ModalFooter,
	ModalHeader,
	ModalTitle,
} from './../../components/bootstrap/Modal';
import Card, {
	CardActions,
	CardBody,
	CardCodeView,
	CardFooter,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from './../../components/bootstrap/Card';
import CommonDesc from './../../common/other/CommonDesc';
import Button from './../../components/bootstrap/Button';
import { TModalFullScreen,  } from './../../type/modal-type';
const ModalCenter = () => {
    const [centeredStatus, setCenteredStatus] = useState(false);
    const [state, setState] = useState(false);
    const [staticBackdropStatus, setStaticBackdropStatus] = useState(false);
    const [scrollableStatus, setScrollableStatus] = useState(false);

	const [sizeStatus, setSizeStatus] = useState(null);
	// const [fullScreenStatus, setFullScreenStatus] = useState<TModalFullScreen | undefined>(
	// 	undefined,
	// );
	const [animationStatus, setAnimationStatus] = useState(true);
	const [longContentStatus, setLongContentStatus] = useState(false);
	const [headerCloseStatus, setHeaderCloseStatus] = useState(true);

	const initialStatus = () => {
		setStaticBackdropStatus(false);
		setScrollableStatus(false);
		setCenteredStatus(false);
		setSizeStatus(null);
		setFullScreenStatus(undefined);
		setAnimationStatus(true);
		setLongContentStatus(false);
		setHeaderCloseStatus(true);
	};
  return (
    <div>	<div className='col-lg-6'>
    <Card stretch>
        {/* <CardHeader>
            <CardLabel icon='CenterFocusStrong'>
                <CardTitle>isCentered</CardTitle>
                <CardSubTitle>Modal</CardSubTitle>
            </CardLabel>
            <CardActions>
                <CommonStoryBtn to='/story/components-modal--default&args=isCentered:true' />
            </CardActions>
        </CardHeader>
        <CardHeader>
            <CommonHowToUse>isCentered: PropTypes.bool,</CommonHowToUse>
        </CardHeader> */}
        <CardBody>
            <Button
                className='me-4'
                color='primary'
                isLight
                icon='Send'
                onClick={() => {
                    // initialStatus();
                    setCenteredStatus(true);
                    setState(true);
                }}>
                Open Modal
            </Button>
        </CardBody>
        <CardFooter>
            <CommonDesc>
                Add isCentered to vertically center the modal.
            </CommonDesc>
        </CardFooter>
    </Card>
    
    <Modal
					isOpen={state}
					setIsOpen={setState}
					titleId='exampleModalLabel'
					isStaticBackdrop={staticBackdropStatus}
					isScrollable={scrollableStatus}
					isCentered={centeredStatus}
					size={sizeStatus}
					// fullScreen={fullScreenStatus}
					isAnimation={animationStatus}>
					<ModalHeader setIsOpen={headerCloseStatus ? setState : undefined}>
						<ModalTitle id='exampleModalLabel'>Modal title</ModalTitle>
					</ModalHeader>
					<ModalBody>aswad</ModalBody>
					<ModalFooter>
						<Button
							color='info'
							isOutline
							className='border-0'
							onClick={() => setState(false)}>
							Close
						</Button>
						<Button color='info' icon='Save'>
							Save changes
						</Button>
					</ModalFooter>
				</Modal>
</div></div>
  )
}

export default ModalCenter