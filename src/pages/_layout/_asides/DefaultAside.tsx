import React, { ReactNode, useContext, useState } from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import Brand from '../../../layout/Brand/Brand';
import Navigation, { NavigationLine } from '../../../layout/Navigation/Navigation';
import User from '../../../layout/User/User';
import {
	dashboardPagesMenu,
	demoPagesMenu,
	pageLayoutTypesPagesMenu,
	superAdminPageLayoutTypesPagesMenu,
} from '../../../menu';
import ThemeContext from '../../../contexts/themeContext';
import Icon from '../../../components/icon/Icon';
import Aside, { AsideBody, AsideFoot, AsideHead } from '../../../layout/Aside/Aside';
import Popovers from '../../../components/bootstrap/Popovers';
import AuthContext from '../../../contexts/authContext';

const DefaultAside = () => {
	const { asideStatus, setAsideStatus } = useContext(ThemeContext);
	const { userData } = useContext(AuthContext);

	const [isSuperAdmin, setIsSuperAdmin] = useState(userData.is_super_admin == 'Y' ? true : false);
	const [doc, setDoc] = useState(
		localStorage.getItem('facit_asideDocStatus') === 'true' || false,
	);

	const { t } = useTranslation(['translation', 'menu']);
 
	return (
		<Aside>
			<AsideHead>
				<Brand asideStatus={asideStatus} setAsideStatus={setAsideStatus} />
			</AsideHead>
			<AsideBody>
				<Navigation menu={dashboardPagesMenu} id='aside-dashboard' />
				<NavigationLine />
				{!isSuperAdmin && (
					<>
						<Navigation menu={pageLayoutTypesPagesMenu} id='aside-menu' />
						<NavigationLine />
					</>
				)}
				{/* {isSuperAdmin && (
					<>
						<Navigation menu={superAdminPageLayoutTypesPagesMenu} id='aside-menu' />
						<NavigationLine />
					</>
				)} */}
				{/* {asideStatus && doc && <div className='p-4'>Documentation</div>} */}
			</AsideBody>
			<AsideFoot>
				<User />
			</AsideFoot>
			{/* <User /> */}
		</Aside>
	);
};

export default DefaultAside;
