import React, { FC, Suspense, useCallback, useContext, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import PageWrapper from '../../../layout/PageWrapper/PageWrapper';
import Page from '../../../layout/Page/Page';
import Card, { CardBody } from '../../../components/bootstrap/Card';
import FormGroup from '../../../components/bootstrap/forms/FormGroup';
import Input from '../../../components/bootstrap/forms/Input';
import Button from '../../../components/bootstrap/Button';
import Logo from '../../../components/Logo';
import useDarkMode from '../../../hooks/useDarkMode';
import { useFormik } from 'formik';
import AuthContext from '../../../contexts/authContext';
import USERS, { IUserProps, getUserDataWithUsername } from '../../../common/data/userDummyData';
import Spinner from '../../../components/bootstrap/Spinner';
import Alert from '../../../components/bootstrap/Alert';
import { FormDataHelper } from '../../../services/FormDataHelper';
import { GeneralServices } from '../../../services/General.Services';
import axios from '../../../services/axiosConfig';
import { ADMIN_LOGIN, ADMIN_SIGNUP, LIST_ADMINS } from '../../../services/axiousURL';
import Select from '../../../components/bootstrap/forms/Select';
import showNotification from '../../../components/extras/showNotification';
import { useAlert } from '../../../hooks/useAlert';
import regexs from '../../../constant/Regexs';
import { dark } from 'react-syntax-highlighter/dist/esm/styles/prism';

interface ILoginHeaderProps {
	isNewUser?: boolean;
}

const LoginHeader: FC<ILoginHeaderProps> = ({ isNewUser }) => {
	// const { setUser, userData, setUserData, user } = useContext(AuthContext);
// const navigate = useNavigate();
// if(userData){
// 	navigate("/")
// }
	if (isNewUser) {
		return (
			<>
				<div className='text-center h1 fw-bold mt-5'>Create Account,</div>
				<div className='text-center h4 text-muted mb-5'>Sign up to get started!</div>
			</>
		);
	}
	return (
		<>
			<div className='text-center h1 fw-bold mt-5'>Welcome,</div>
			<div className='text-center h4 text-muted mb-5'>Sign in to continue!</div>
		</>
	);
};

interface ILoginProps {
	isSignUp?: boolean;
}
const Login: FC<ILoginProps> = ({ isSignUp }) => {
	const { setUser, userData, setUserData, user } = useContext(AuthContext);
	
	const [AlertMessage, isVisible, getProps] = useAlert();
	const { darkModeStatus } = useDarkMode();

	//  const { emailRegex, } =regixs
	
	const [signInPassword, setSignInPassword] = useState<boolean>(false);
	const [singUpStatus, setSingUpStatus] = useState<boolean>(!!isSignUp);
	const [placeholderText, setPlaceholderText] = useState('Your pharmacy name');

	const navigate = useNavigate();
	const handleOnClick = useCallback(() => navigate('/'), [navigate]);

	const usernameCheck = (username: string) => {
		return !!getUserDataWithUsername(username);
	};

	const passwordCheck = (username: string, password: string) => {
		return getUserDataWithUsername(username).password === password;
	};

	const TYPE_OPTIONS = [
		{ value: 'Pharmacy', text: 'Pharmacy' },
		{ value: 'Labs', text: 'Labs' },
		{ value: 'Hospitals', text: 'Hospitals' },
	];

	const formik = useFormik({
		enableReinitialize: true,
		initialValues: {
			loginUsername: '',
			loginPassword: '',
			singupPharmacyName: '',
			singupPhoneNumber: '',
			singupPassword: '',
			singupUsername: '',
			singupEmail: '',
			singupType: '',
		},
		validate: (values) => {
			const errors: {
				loginUsername?: string;
				loginPassword?: string;
				singupPharmacyName?: string;
				singupPhoneNumber?: string;
				singupPassword?: string;
				singupUsername?: string;
				singupEmail?: string;
				singupType?: string;
			} = {};

			if (singUpStatus) {
				if (!values.singupEmail) {
					errors.singupEmail = 'Required';
				} else if (!regexs.emailRegex.regex.test(values.singupEmail)) {
					errors.singupEmail = regexs.emailRegex.errMessage;
				}

				if (!values.singupUsername) {
					errors.singupUsername = 'Required';
				} else if (!regexs.usernameRegex.regex.test(values.singupUsername)) {
					errors.singupUsername = regexs.usernameRegex.errMessage;
				}

				if (!values.singupPassword) {
					errors.singupPassword = 'Required';
				} else if (!regexs.passwordRegex.regex.test(values.singupPassword)) {
					errors.singupPassword = regexs.passwordRegex.errMessage;
				}
				if (!values.singupPhoneNumber) {
					errors.singupPhoneNumber = 'Required';
				} else if (!regexs.phoneRegex.regex.test(values.singupPhoneNumber)) {
					errors.singupPhoneNumber = regexs.phoneRegex.errMessage;
				}

				if (!values.singupPharmacyName) {
					errors.singupPharmacyName = 'Required';
				} else if (!regexs.pharmacyNameRegex.regex.test(values.singupPharmacyName.trim())) {
					errors.singupPharmacyName = regexs.pharmacyNameRegex.errMessage;
				}
				if (!values.singupType) {
					errors.singupType = 'Required';
				}
			} else {
				if (!values.loginUsername) {
					errors.loginUsername = 'Required';
				} else if (!regexs.emailRegex.regex.test(values.loginUsername)) {
					errors.loginUsername = regexs.emailRegex.errMessage;
				}

				if (!values.loginPassword) {
					errors.loginPassword = 'Required';
				}
			}

			return errors;
		},
		validateOnChange: true,
		onSubmit: (values) => {
			if (singUpStatus) {
				let formDataHelper = FormDataHelper();
				formDataHelper.append('email_address', values.singupEmail);
				formDataHelper.append('user_name', values.singupUsername);
				formDataHelper.append('password', values.singupPassword);
				formDataHelper.append('confirm_password', values.singupPassword);
				formDataHelper.append('phone_number', values.singupPhoneNumber);
				formDataHelper.append('pharmacy_title', values.singupPharmacyName);
				formDataHelper.append('is_admin', 'N');
				formDataHelper.append('pharmacy_description', '');
				formDataHelper.append('type', values.singupType);

				GeneralServices.postRequest(formDataHelper, ADMIN_SIGNUP).then(
					(resp) => {
						formDataHelper = FormDataHelper();
						formDataHelper.append('email_address', values.singupEmail);
						formDataHelper.append('password', values.singupPassword);

						GeneralServices.postRequest(formDataHelper, ADMIN_LOGIN).then(
							(admingLoginResp) => {
								if (setUserData) setUserData(admingLoginResp.data[0]);
								if (setUser) setUser(admingLoginResp.data[0].user_name);
								handleOnClick();
							},
							(adminLoginErr) => {
								console.log(adminLoginErr);
							},
						);
					},
					(err) => {
						getProps({ variant: 'danger', message: err });
						// showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
					},
				);
				return;
			}

			let formDataHelper = FormDataHelper();
			formDataHelper.append('email_address', values.loginUsername);
			formDataHelper.append('password', values.loginPassword);

			GeneralServices.postRequest(formDataHelper, ADMIN_LOGIN).then(
				(resp) => {
					console.log(resp.data[0]);
					if (setUserData) setUserData(resp.data[0]);
					if (setUser) setUser(resp.data[0].user_name);
					handleOnClick();
				},
				(err) => {
					formik.setFieldError('loginPassword', 'Username and password do not match.');
					console.log(err);
				},
			);

			// if (usernameCheck(values.loginUsername)) {
			// 	if (passwordCheck(values.loginUsername, values.loginPassword)) {
			// 		if (setUser) {
			// 			setUser(values.loginUsername);
			// 		}

			// 		let formDataHelper = FormDataHelper();
			// 		formDataHelper.append('email_address', formik.values.loginUsername);
			// 		formDataHelper.append('password', formik.values.loginPassword);

			// 		axios({ method: 'POST', url: ADMIN_LOGIN, data: formDataHelper }).then(
			// 			(resp) => {
			// 				console.log(resp);
			// 				// if (setUserData) {
			// 				// 	setUserData(resp);
			// 				// }
			// 			},
			// 			(err) => {
			// 				formik.setFieldError(
			// 					'loginPassword',
			// 					'Username and password do not match.',
			// 				);
			// 				console.log(err);
			// 			},
			// 		);
			// 		// handleOnClick();
			// 	} else {
			// 		formik.setFieldError('loginPassword', 'Username and password do not match.');
			// 	}
			// }
		},
	});

	const [isLoading, setIsLoading] = useState<boolean>(false);
	const handleContinue = () => {
		setIsLoading(true);

		let formDataHelper = FormDataHelper();
		formDataHelper.append('email_address', formik.values.loginUsername);
		GeneralServices.postRequest(formDataHelper, LIST_ADMINS).then(
			(resp) => {
				if (resp.data.length > 0) {
					setSignInPassword(true);
				} else {
					formik.setFieldError('loginUsername', 'No such user found in the system.');
					setIsLoading(false);
				}
			},
			(err) => {
				formik.setFieldError('loginUsername', 'No such user found in the system.');
				setIsLoading(false);
			},
		);

		// setTimeout(() => {
		// 	if (
		// 		!Object.keys(USERS).find(
		// 			(f) => USERS[f].username.toString() === formik.values.loginUsername,
		// 		)
		// 	) {
		// 		formik.setFieldError('loginUsername', 'No such user found in the system.');
		// 	} else {
		// 		setSignInPassword(true);
		// 	}
		// 	setIsLoading(false);
		// }, 1000);
	};

	return (
		<PageWrapper
			isProtected={false}
			title={singUpStatus ? 'Sign Up' : 'Login'}
			// className={classNames({ 'bg-dark': !singUpStatus, 'bg-light': singUpStatus })}>
			className={classNames({ 'bg-light': true })}>
			<Page className='p-0'>
				<div className='row h-100 align-items-center justify-content-center'>
					<div className='col-xl-4 col-lg-6 col-md-8 shadow-3d-container'>
						<Card className='shadow-3d-dark' data-tour='login-page'>
							<CardBody>
								<div className='text-center my-5'>
									<Link
										to='/'
										className={classNames(
											'text-decoration-none  fw-bold display-2',
											{
												'text-dark': !darkModeStatus,
												'text-light': darkModeStatus,
											},
										)}>
										<Logo width={200} />
									</Link>
								</div>
								<div
									className={classNames('rounded-3', {
										'bg-l10-dark': !darkModeStatus,
										'bg-dark': darkModeStatus,
									})}>
									<div className='row row-cols-2 g-3 pb-3 px-3 mt-0'>
										<div className='col'>
											<Button
												color={darkModeStatus ? 'light' : 'dark'}
												isLight={singUpStatus}
												className='rounded-1 w-100'
												size='lg'
												onClick={() => {
													setSignInPassword(false);
													setSingUpStatus(!singUpStatus);
												}}>
												Login
											</Button>
										</div>
										<div className='col'>
											<Button
												color={darkModeStatus ? 'light' : 'dark'}
												isLight={!singUpStatus}
												className='rounded-1 w-100'
												size='lg'
												onClick={() => {
													setSignInPassword(false);
													setSingUpStatus(!singUpStatus);
												}}>
												Sign Up
											</Button>
										</div>
									</div>
								</div>

								<LoginHeader isNewUser={singUpStatus} />
								{/* <Alert isLight icon='Lock' isDismissible>
									<div className='row'>
										<div className='col-12'>
											<strong>Username:</strong> {`a@b.com`}
										</div>
										<div className='col-12'>
											<strong>Password:</strong> {`123`}
										</div>
									</div>
								</Alert>
								{isVisible ? <AlertMessage /> : ''} */}
								<form className='row g-4'>
									{singUpStatus ? (
										<>
											<div className='col-12'>
												<FormGroup
													id='singupEmail'
													isFloating
													label='Your email'>
													<Input
														type='email'
														autoComplete='email'
														value={formik.values.singupEmail}
														isTouched={formik.touched.singupEmail}
														invalidFeedback={formik.errors.singupEmail}
														validFeedback='Looks good!'
														isValid={formik.isValid}
														onChange={formik.handleChange}
														onBlur={formik.handleBlur}
													/>
												</FormGroup>
											</div>
											<div className='col-12'>
												<FormGroup
													id='singupUsername'
													isFloating
													label='Your username'>
													<Input
														autoComplete='family-name'
														value={formik.values.singupUsername}
														isTouched={formik.touched.singupUsername}
														invalidFeedback={
															formik.errors.singupUsername
														}
														validFeedback='Looks good!'
														isValid={formik.isValid}
														onChange={formik.handleChange}
														onBlur={formik.handleBlur}
													/>
												</FormGroup>
											</div>
											<div className='col-12'>
												<FormGroup
													id='singupPassword'
													isFloating
													label='Password'>
													<Input
														type='password'
														autoComplete='password'
														value={formik.values.singupPassword}
														isTouched={formik.touched.singupPassword}
														invalidFeedback={
															formik.errors.singupPassword
														}
														validFeedback='Looks good!'
														isValid={formik.isValid}
														onChange={formik.handleChange}
														onBlur={formik.handleBlur}
													/>
												</FormGroup>
											</div>
											<div className='col-12'>
												<FormGroup
													id='singupPhoneNumber'
													isFloating
													label='Your phone number'>
													<Input
														value={formik.values.singupPhoneNumber}
														isTouched={formik.touched.singupPhoneNumber}
														invalidFeedback={
															formik.errors.singupPhoneNumber
														}
														validFeedback='Looks good!'
														isValid={formik.isValid}
														onChange={formik.handleChange}
														onBlur={formik.handleBlur}
													/>
												</FormGroup>
											</div>

											<div className='col-12'>
												<FormGroup
													id='singupType'
													label='Please Select Type'>
													<Select
														ariaLabel='type'
														required
														placeholder='---Select Type---'
														onChange={(event: any) => {
															let value = event.target.value;
															console.log(value);
															if (value == 'Labs') {
																setPlaceholderText('Your lab name');
															} else if (value == 'Hospitals') {
																setPlaceholderText(
																	'Your hospital name',
																);
															} else {
																setPlaceholderText(
																	'Your pharmacy name',
																);
															}
															formik.handleChange(event);
														}}
														value={formik.values.singupType}
														list={TYPE_OPTIONS}
														isTouched={formik.touched.singupType}
														invalidFeedback={formik.errors.singupType}
														validFeedback='Looks good!'
													/>
												</FormGroup>
											</div>

											<div className='col-12'>
												<FormGroup
													id='singupPharmacyName'
													isFloating
													label={placeholderText}>
													<Input
														value={formik.values.singupPharmacyName}
														isTouched={
															formik.touched.singupPharmacyName
														}
														invalidFeedback={
															formik.errors.singupPharmacyName
														}
														validFeedback='Looks good!'
														isValid={formik.isValid}
														onChange={formik.handleChange}
														onBlur={formik.handleBlur}
													/>
												</FormGroup>
											</div>

											<div className='col-12'>
												<Button
													color='info'
													className='w-100 py-3'
													// onClick={handleOnClick}>
													onClick={formik.handleSubmit}>
													Sign Up
												</Button>
											</div>
										</>
									) : (
										<>
											<div className='col-12'>
												<FormGroup
													id='loginUsername'
													isFloating
													label='Your email or username'
													style={{color:'blue' }}
													className={classNames({
														'd-none': signInPassword,
													})}>
													<Input
														autoComplete='username'
														value={formik.values.loginUsername}
														isTouched={formik.touched.loginUsername}
														invalidFeedback={
															formik.errors.loginUsername
														}
														isValid={formik.isValid}
														onChange={formik.handleChange}
														onBlur={formik.handleBlur}
														onFocus={() => {
															formik.setErrors({});
														}}
													/>
												</FormGroup>
												{signInPassword && (
													<div className='text-center h4 mb-3 fw-bold'>
														Hi, {formik.values.loginUsername}.
													</div>
												)}
												<FormGroup
													id='loginPassword'
													isFloating
													label='Password'
													className={classNames({
														'd-none': !signInPassword,
													})}>
													<Input
														type='password'
														autoComplete='current-password'
														value={formik.values.loginPassword}
														isTouched={formik.touched.loginPassword}
														invalidFeedback={
															formik.errors.loginPassword
														}
														validFeedback='Looks good!'
														isValid={formik.isValid}
														onChange={formik.handleChange}
														onBlur={formik.handleBlur}
													/>
												</FormGroup>
											</div>
											<div className='col-12'>
												{!signInPassword ? (
													<Button
														color='warning'
														className='w-100 py-3'
														isDisable={!formik.values.loginUsername}
														onClick={handleContinue}>
														{isLoading && (
															<Spinner isSmall inButton isGrow />
														)}
														Continue
													</Button>
												) : (
													<>
														<Button
															color='warning'
															className='w-100 py-3'
															onClick={formik.handleSubmit}>
															Login
														</Button>
														<div><Button
														className='my-1'
															color='primary'
															isLink
															icon='ArrowBack'
															tag='a'
															// to={`../${pageLayoutTypesPagesMenu.orders.subMenu.listOrders.path}`}
															onClick={() => {
																setIsLoading(false);
																setSignInPassword(false);
															}}>
															Back to email
														</Button></div>
														
													</>
												)}
											</div>
										</>
									)}

									{/* BEGIN :: Social Login */}
									{!signInPassword && (
										<>
											{/* <div className='col-12 mt-3 text-center text-muted'>
												OR
											</div> */}
											{/* <div className='col-12 mt-3'>
												<Button
													isOutline
													color={darkModeStatus ? 'light' : 'dark'}
													className={classNames('w-100 py-3', {
														'border-light': !darkModeStatus,
														'border-dark': darkModeStatus,
													})}
													icon='CustomApple'
													onClick={handleOnClick}>
													Sign in with Apple
												</Button>
											</div>
											<div className='col-12'>
												<Button
													isOutline
													color={darkModeStatus ? 'light' : 'dark'}
													className={classNames('w-100 py-3', {
														'border-light': !darkModeStatus,
														'border-dark': darkModeStatus,
													})}
													icon='CustomGoogle'
													onClick={handleOnClick}>
													Continue with Google
												</Button>
											</div> */}
										</>
									)}
									{/* END :: Social Login */}
								</form>
							</CardBody>
						</Card>
						<div className='text-center'>
							<a
								href='/'
								className={classNames('text-decoration-none me-3', {
									'link-light': singUpStatus,
									'link-dark': !singUpStatus,
								})}>
								Privacy policy
							</a>
							<a
								href='/'
								className={classNames('link-light text-decoration-none', {
									'link-light': singUpStatus,
									'link-dark': !singUpStatus,
								})}>
								Terms of use
							</a>
						</div>
					</div>
				</div>
			</Page>
		</PageWrapper>
	);
};
Login.propTypes = {
	isSignUp: PropTypes.bool,
};
Login.defaultProps = {
	isSignUp: false,
};

export default Login;
