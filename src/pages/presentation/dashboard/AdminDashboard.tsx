import React from 'react';
import CommonSalePerformance from './CommonSalePerformance';
import CommonTopSales from './CommonTopSales';
import CardFooterInner from '../../_common/CardFooterInner';
import CardEmpDashboard from '../../_common/CardEmployee';

const AdminDashboard = () => {
	return (
		<div className='row'>
			{/* <div className='col-lg-8'>
				<CommonSalePerformance />
			</div>
			<div className='col-lg-4'>
				<CommonTopSales />
			</div> */}
			{/* <CardFooterInner/> */}
			<CardEmpDashboard/>
		</div>
	);
};

export default AdminDashboard;
