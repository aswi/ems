import React from 'react';
import SubHeader, { SubHeaderLeft, SubHeaderRight } from '../../../layout/SubHeader/SubHeader';
import Breadcrumb from '../../../components/bootstrap/Breadcrumb';
import Page from '../../../layout/Page/Page';
import PageWrapper from '../../../layout/PageWrapper/PageWrapper';
import RadialBarBasic from './../../../chart/chart-radial-bar/RadialBarBasic';

import CommonStoryBtn from '../../../common/other/CommonStoryBtn';
// import { superAdminPageLayoutTypesPagesMenu } from './../../../menu';

const ChartRadialBarPage = () => {
	return (
		<PageWrapper>
			<Page>
				<div className='row'>
					<RadialBarBasic />
					<RadialBarBasic />
				</div>
			</Page>
		</PageWrapper>
	);
};

export default ChartRadialBarPage;
