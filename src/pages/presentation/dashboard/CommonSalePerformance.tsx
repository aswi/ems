import React, { useContext, useEffect, useState } from 'react';
import dayjs from 'dayjs';
import Card, {
	CardActions,
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../components/bootstrap/Card';
import Button from '../../../components/bootstrap/Button';
import Chart, { IChartOptions } from '../../../components/extras/Chart';
import { GeneralServices } from '../../../services/General.Services';
import { FormDataHelper } from '../../../services/FormDataHelper';
import AuthContext from '../../../contexts/authContext';
import {
	DAILY_SALE_REPORT,
	MONTHLY_SALE_REPORT,
	TOP_SALES_REPORT,
	WEEKLY_SALE_REPORT,
} from '../../../services/axiousURL';

interface ISalePerTab {
	[key: string]: 'Day' | 'Week' | 'Month';
}
const CommonSalePerformance = () => {
	const { userData } = useContext(AuthContext);
	const [currentDaySaleData, setCurrentDaySaleData] = useState<any>([]);
	const [prevDaySaleData, setPrevDaySaleData] = useState<any>([]);

	const [currentWeekSaleData, setCurrentWeekSaleData] = useState<any>([]);
	const [prevWeekSaleData, setPrevWeekSaleData] = useState<any>([]);

	const [currentMonthSaleData, setCurrentMonthSaleData] = useState<any>([]);
	const [prevMonthSaleData, setPrevMonthSaleData] = useState<any>([]);

	useEffect(() => {
		let helper = FormDataHelper();
		helper.append('pharmacy_id', userData.pharmacy_id!);
		GeneralServices.postRequest(helper, DAILY_SALE_REPORT).then(
			(resp) => {
				let currentDay = resp.data.current_day;
				let prevDay = resp.data.previous_day;

				setCurrentDaySaleData(currentDay);
				setPrevDaySaleData(prevDay);
			},
			(err) => {},
		);
	}, [userData.pharmacy_id]);

	useEffect(() => {
		let helper = FormDataHelper();
		helper.append('pharmacy_id', userData.pharmacy_id!);
		GeneralServices.postRequest(helper, WEEKLY_SALE_REPORT).then(
			(resp) => {
				let currentWeek = resp.data.current_week;
				let prevWeek = resp.data.previous_week;
                   
				setCurrentWeekSaleData(currentWeek);
				setPrevWeekSaleData(prevWeek);
			},
			(err) => {},
		);
	}, [userData.pharmacy_id]);

	useEffect(() => {
		let helper = FormDataHelper();
		helper.append('pharmacy_id', userData.pharmacy_id!);
		GeneralServices.postRequest(helper, MONTHLY_SALE_REPORT).then(
			(resp) => {
				let currentMonth = resp.data.current_month;
				let prevMonth = resp.data.previous_month;
              
 
				setCurrentMonthSaleData(currentMonth);
				setPrevMonthSaleData(prevMonth);
			},
			(err) => {},
		);
	}, [userData.pharmacy_id]);

	useEffect(() => {
		setActiveSalePerTab(SALE_PER_TAB.MONTH);
		setState({
			series: DUMMY_DATA.MONTH.series,
			options: DUMMY_DATA.MONTH.options,
		});
	}, [currentMonthSaleData]); // eslint-disable-line react-hooks/exhaustive-deps

	const chartOptions = {
		chart: {
			height: 400,
			type: 'area',
			toolbar: {
				show: false,
			},
		},
		colors: [process.env.REACT_APP_SUCCESS_COLOR, process.env.REACT_APP_INFO_COLOR],
		dataLabels: {
			enabled: false,
		},
		stroke: {
			curve: 'smooth',
		},
		tooltip: {
			x: {
				format: 'dd/MM/yy HH:mm',
			},
			theme: 'dark',
		},
		fill: {
			type: 'gradient',
			gradient: {
				shadeIntensity: 1,
				opacityFrom: 0.5,
				opacityTo: 0,
				stops: [0, 100],
			},
		},
	};

	function getMonthDates(): string[] {
		const startOfMonth = dayjs().startOf('month');
		const endOfMonth = dayjs().endOf('month');
		const numDays = endOfMonth.diff(startOfMonth, 'day') + 1;

		return Array.from({ length: numDays }).map((_, index) =>
			startOfMonth.add(index, 'day').format('ll')
		);
	}

	function getWeekDates() {
		
		const startOfWeek = dayjs().startOf('week').add(1, 'day'); // Start from Monday
		const endOfWeek = dayjs().endOf('week').add(1, 'day'); // Adjust end of week accordingly
		const numDays = endOfWeek.diff(startOfWeek, 'day') + 1;

		return Array.from({ length: numDays }).map((_, index) =>
		  startOfWeek.add(index, 'day').format('ll')
		);
	  }
	  
	  
	  

	const DUMMY_DATA: { [key: string]: IChartOptions } = {
		DAY: {
			series: [
				{
					name: 'Last Day',
					data: prevDaySaleData,
				},
				{
					name: 'This Day',
					data: currentDaySaleData,
				},
			],
			// @ts-ignore
			options: {
				...chartOptions,
				xaxis: {
					categories: Array.from({ length: 24 }).map((_, index) => index.toString()),
				},
			},
		},
		WEEK: {
			series: [
				{
					name: 'Last Week',
					data: prevWeekSaleData,
				},
				{
					name: 'This Week',
					data: currentWeekSaleData,
				},
			],
			// @ts-ignore
			options: {
				...chartOptions,
				xaxis: {
					categories: getWeekDates(),
				},
			},
		},
		MONTH: {
			series: [
				{
					name: 'Last Month',
					data: prevMonthSaleData,
				},
				{
					name: 'This Month',
					data: currentMonthSaleData,
				},
			],
			// @ts-ignore
			options: {
				...chartOptions,
				xaxis: {
					categories: getMonthDates(),
				},
			},
		},
	};

	const [state, setState] = useState<IChartOptions>({
		series: DUMMY_DATA.WEEK.series,
		options: DUMMY_DATA.WEEK.options,
	});

	const SALE_PER_TAB: { [key: string]: ISalePerTab['key'] } = {
		DAY: 'Day',
		WEEK: 'Week',
		MONTH: 'Month',
	};
	const [activeSalePerTab, setActiveSalePerTab] = useState<ISalePerTab['key']>(
		SALE_PER_TAB.MONTH
	);

	return (
		<Card stretch>
			<CardHeader>
				<CardLabel>
					<CardTitle>Sale Performance</CardTitle>
				</CardLabel>
				<CardActions>
					<Button
						color='info'
						onClick={() => {
							setActiveSalePerTab(SALE_PER_TAB.DAY);
							setState({
								series: DUMMY_DATA.DAY.series,
								options: DUMMY_DATA.DAY.options,
							});
						}}
						isLink={activeSalePerTab !== SALE_PER_TAB.DAY}
						isLight={activeSalePerTab === SALE_PER_TAB.DAY}
					>
						Day
					</Button>
					<Button
						color='info'
						onClick={() => {
							setActiveSalePerTab(SALE_PER_TAB.WEEK);
							setState({
								series: DUMMY_DATA.WEEK.series,
								options: DUMMY_DATA.WEEK.options,
							});
						}}
						isLink={activeSalePerTab !== SALE_PER_TAB.WEEK}
						isLight={activeSalePerTab === SALE_PER_TAB.WEEK}
					>
						Week
					</Button>
					<Button
						color='info'
						onClick={() => {
							setActiveSalePerTab(SALE_PER_TAB.MONTH);
							setState({
								series: DUMMY_DATA.MONTH.series,
								options: DUMMY_DATA.MONTH.options,
							});
						}}
						isLink={activeSalePerTab !== SALE_PER_TAB.MONTH}
						isLight={activeSalePerTab === SALE_PER_TAB.MONTH}
					>
						Month
					</Button>
				</CardActions>
			</CardHeader>
			<CardBody>
				{currentWeekSaleData.length > 0 && (
					<Chart
						series={state.series}
						options={state.options}
						type={state.options.chart?.type}
						height={state.options.chart?.height}
					/>
				)}
			</CardBody>
		</Card>
	);
};

export default CommonSalePerformance;
