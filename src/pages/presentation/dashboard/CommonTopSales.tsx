import React, { FC, useContext, useEffect, useState } from 'react';
import classNames from 'classnames';
import Card, {
	CardActions,
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../components/bootstrap/Card';
import Button from '../../../components/bootstrap/Button';
import topSalesData from './dummySalesData';
import { getFirstLetter, priceFormat } from '../../../helpers/helpers';
import useDarkMode from '../../../hooks/useDarkMode';
import { demoPagesMenu } from '../../../menu';
import { FormDataHelper } from '../../../services/FormDataHelper';
import AuthContext from '../../../contexts/authContext';
import { GeneralServices } from '../../../services/General.Services';
import { TOP_SALES_REPORT } from '../../../services/axiousURL';

interface ITopSalesItemProps {
	name: string;
	type: string;
	price: number;
	count: number;
}
const TopSalesItem: FC<ITopSalesItemProps> = ({ name, type = 'sales', price, count }) => {
	const { darkModeStatus } = useDarkMode();

	return (
		<div className='col-12'>
			<div className='row'>
				<div className='col d-flex align-items-center'>
					<div className='flex-shrink-0'>
						<div className='ratio ratio-1x1 me-3' style={{ width: 48 }}>
							<div
								className={classNames(
									'rounded-2',
									'd-flex align-items-center justify-content-center',
									{
										'bg-l10-dark': !darkModeStatus,
										'bg-l90-dark': darkModeStatus,
									},
								)}>
								<span className='fw-bold'>{getFirstLetter(name)}</span>
							</div>
						</div>
					</div>
					<div className='flex-grow-1'>
						<div className='fs-6'>{name}</div>
						<div className='text-muted'>
							<small>{type}</small>
						</div>
					</div>
				</div>
				<div className='col-auto text-end'>
					<div>
						<strong>{priceFormat(price)}</strong>
					</div>
					<div className='text-muted'>
						<small>{count}</small>
					</div>
				</div>
			</div>
		</div>
	);
};

const CommonTopSales = () => {
	const { userData } = useContext(AuthContext);
	const [topSales, setTopSales] = useState<any[]>([]);
	useEffect(() => {
		if (!userData.pharmacy_id) return;
		let helper = FormDataHelper();
		helper.append('pharmacy_id', userData.pharmacy_id);
		GeneralServices.postRequest(helper, TOP_SALES_REPORT).then(
			(resp) => {
				setTopSales(resp.data);
			},
			(err) => {},
		);
	}, [userData.pharmacy_id]);

	return (
		<Card stretch>
			<CardHeader>
				<CardLabel>
					<CardTitle>Top Sales</CardTitle>
				</CardLabel>
			</CardHeader>
			<CardBody isScrollable>
				<div className='row g-3'>
					{topSales.map((i, key) => (
						<TopSalesItem key={key} {...i} />
					))}
				</div>
			</CardBody>
		</Card>
	);
};

export default CommonTopSales;
