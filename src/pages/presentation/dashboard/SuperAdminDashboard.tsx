import React from 'react';
import CardFooterInner from '../../_common/CardFooterInner';
import ChartSparklinePage from '../../../chart/sparkline/ChartSparklinePage';

const SuperAdminDashboard = () => {
	return (
		<div className='row'>
			<CardFooterInner />
			<ChartSparklinePage />
		</div>
	);
};

export default SuperAdminDashboard;
