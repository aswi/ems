import React, { useContext, useEffect, useState } from 'react';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import SubHeader, { SubHeaderLeft } from '../../../../layout/SubHeader/SubHeader';
import Button from '../../../../components/bootstrap/Button';
import Page from '../../../../layout/Page/Page';
import useDarkMode from '../../../../hooks/useDarkMode';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import PaginationButtons, {
	PER_COUNT,
	dataPagination,
} from '../../../../components/PaginationButtons';
import Card, {
	CardActions,
	CardBody,
	CardFooter,
	CardFooterRight,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import Avatar from '../../../../components/Avatar';
import { getColorNameWithIndex } from '../../../../common/data/enumColors';
import Icon from '../../../../components/icon/Icon';
import dayjs from 'dayjs';
import { priceFormat } from '../../../../helpers/helpers';
import UserImage3 from '../../../../assets/img/wanna/wanna3.png';
import UserImage3Webp from '../../../../assets/img/wanna/wanna3.webp';
import useSortableData from '../../../../hooks/useSortableData';
import { pageLayoutTypesPagesMenu } from '../../../../menu';
import { GeneralServices } from '../../../../services/General.Services';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { LIST_ORDERS } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import PlaceholderImage from '../../../../components/extras/PlaceholderImage';
import { downloadImage } from '../../../../helpers/helpers';
import { toDate } from 'date-fns/fp';
import CardFooterInner from '../../../_common/CardFooterInner';

const Order = () => {
	const { darkModeStatus } = useDarkMode();
	const location = useLocation();
	const navigate = useNavigate();

	const orderDetail = location.state;

	function formatPriceWithCommas(price: any) {
		let priceStr = price?.toString();
		priceStr = priceStr?.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

		return priceStr;
	}

	const totalSpending = formatPriceWithCommas(orderDetail?.total_spending);

	const [currentPage, setCurrentPage] = useState<number>(1);
	const [perPage, setPerPage] = useState<number>(PER_COUNT['3']);
	const [onPageView, setOnPageView] = useState<boolean>(false);
	const [order, setOrder] = useState<any>({});
	const [products, setProducts] = useState<any[]>([]);
	const { items, requestSort, getClassNamesFor } = useSortableData(products);

	const [editModalStatus, setEditModalStatus] = useState<boolean>(false);
	

	useEffect(() => {
		if (!orderDetail || !orderDetail.order_detail) return;
		let orderProducts: any[] = [];
		orderDetail?.order_detail.forEach((single: any) => {
			let productQty: any = single.product_quantity;
			single.product_info[0].productQty = productQty;
			orderProducts.push(single.product_info[0]);
		});
		setProducts(orderProducts);
	}, [order]); // eslint-disable-line react-hooks/exhaustive-deps

	const { userData } = useContext(AuthContext);
	// useEffect(() => {
	// 	if (!id) return;

	// 	let helper = FormDataHelper();
	// 	helper.append('id', id);
	// 	helper.append('pharmacy_id', userData.pharmacy_id!);
	// 	GeneralServices.postRequest(helper, LIST_ORDERS).then(
	// 		(resp) => {
	// 			console.log('mmmmmm', resp.data);
	// 			console.log(resp.data[0]);
	// 			setOrder(resp.data[0]);
	// 		},
	// 		(err) => {

	// 		},
	// 	);
	// }, [id, userData.pharmacy_id]);
	// console.log('Order', order)
	// useEffect(() => {
	// 	if (orderDetail === null || orderDetail === undefined) {
	// 		return navigate(pageLayoutTypesPagesMenu.attendance.subMenu.listAttendance.path);
	// 	}
	// }, []); // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<PageWrapper title={'Employee Detail'}>
			<SubHeader>
				<SubHeaderLeft>
					<Button
						color='primary'
						isLink
						icon='ArrowBack'
						tag='a'
						to={`../${pageLayoutTypesPagesMenu.attendance.subMenu.listAttendance.path}`}>
						Back to List
					</Button>
				</SubHeaderLeft>
			</SubHeader>
			<Page>
				{/* <div className='pt-3 pb-5 d-flex align-items-center'>
					<span className='display-4 fw-bold me-3'>
						{orderDetail?.customer_first_name + ' ' + orderDetail?.customer_last_name}
					</span>
					<span className='border border-success border-2 text-success fw-bold px-3 py-2 rounded'>
						{'Customer'}
					</span>
				</div> */}
				<div className='row'>
					<div className='col-lg-12'>
						<Card className='shadow-3d-primary '>
							<CardBody>
								<div className='row g-5 py-3'>
									<div className='col-12 d-flex justify-content-between align-items-center'>
										<Avatar
											src={UserImage3}
											srcSet={UserImage3Webp}
											color={getColorNameWithIndex(1)}
										/>
										<div>
											<div className='fw-bold fs-5 mb-0'>Employee Name</div>
											<div className='text-muted'>Aswad Zaman</div>
										</div>
										<div></div>
										<div>
											<div className='fw-bold fs-5 mb-0'>Employee ID</div>
											<div className='text-muted'>IM062587UT</div>
										</div>
										<div>
											<div className='fw-bold fs-5 mb-0'>Joining Date</div>
											<div className='text-muted'>12 January 2015</div>
										</div>
										<div>
											<div className='fw-bold fs-5 mb-0'>Department</div>
											<div className='text-muted'>Accounts</div>
										</div>
									</div>
								</div>
							</CardBody>
							
						</Card>

						<Card className='shadow-3d-primary '>
							<CardBody>
								<div className='row g-5 py-3'>
									<div className='col-12 d-flex justify-content-between align-items-center'>
										{/* <Avatar
											src={UserImage3}
											srcSet={UserImage3Webp}
											color={getColorNameWithIndex(1)}
										/> */}
										<div  className='d-flex flex-column align-items-center'>
											<div className='fw-bold fs-5 mb-4 text-success'>08:00</div>
											<div className='text-muted'>Average Working Hours</div>
										</div>
										
										<div className='d-flex flex-column align-items-center' >
											<div className='fw-bold fs-5 mb-4 text-success'>10:30 AM</div>
											<div className='text-muted'>Average In Time</div>
										</div>
										<div className='d-flex flex-column align-items-center'>
											<div className='fw-bold fs-5 mb-4 text-success'>07:30 PM</div>
											<div className='text-muted'>Average Out Time</div>
										</div>
										<div className='d-flex flex-column align-items-center'>
											<div className='fw-bold fs-5 mb-4 text-success'>01:00</div>
											<div className='text-muted'>Average Break Time</div>
										</div>
									</div>
								</div>
							</CardBody>
							{/* <CardFooter
								borderSize={1}
								borderColor='dark'
								className='mx-3 py-1'>
									 </CardFooter> */}
						</Card>


						
						{/* <CardFooterInner/> */}
						
						
					</div>
					<div className='col-lg-12'>
						<Card>
							<CardHeader>
								<CardLabel icon='Receipt'>
									<CardTitle>Aswad Attendance</CardTitle>
								</CardLabel>
							</CardHeader>
							<CardBody>
								<table className='table table-modern table-hover'>
									<thead>
										<tr>
											<th
												onClick={() => requestSort('name')}
												className='cursor-pointer text-decoration-underline'>
												Date
												<Icon
													size='lg'
													className={getClassNamesFor('name')}
													icon='FilterList'
												/>
											</th>
											<th
												onClick={() => requestSort('category')}
												className='cursor-pointer text-decoration-underline text-center'>
												Check In
												<Icon
													size='lg'
													className={getClassNamesFor('category')}
													icon='FilterList'
												/>
											</th>
											<th
												onClick={() => requestSort('date')}
												className='cursor-pointer text-decoration-underline text-center'>
												Check Out
												<Icon
													size='lg'
													className={getClassNamesFor('quantity')}
													icon='FilterList'
												/>
											</th>
											<th
												onClick={() => requestSort('date')}
												className='cursor-pointer text-decoration-underline text-center'>
												Working Hours
												<Icon
													size='lg'
													className={getClassNamesFor('quantity')}
													icon='FilterList'
												/>
											</th>
											<th
												onClick={() => requestSort('price')}
												className='cursor-pointer text-decoration-underline'>
											Status
												<Icon
													size='lg'
													className={getClassNamesFor('price')}
													icon='FilterList'
												/>
											</th>
										</tr>
									</thead>
									<tbody>
										{dataPagination(items, currentPage, perPage).map((i) => (
											<tr key={i.id}>
												<td>{i.title}</td>
												<td className='text-center'>
													{i.product_category}
												</td>
												<td className='text-center'>{i.price}</td>

												<td className='text-center'>{i.productQty}</td>
												<td>{i.price * i.productQty}</td>
											</tr>
										))}
									</tbody>
								</table>
							</CardBody>
							<PaginationButtons
								data={items}
								label='items'
								setCurrentPage={setCurrentPage}
								currentPage={currentPage}
								perPage={perPage}
								setPerPage={setPerPage}
							/>
						</Card>
					
		
					</div>
				</div>
			</Page>
		</PageWrapper>
	);
};

export default Order;
