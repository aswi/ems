import React, { useState, useEffect, useContext } from 'react';
import { useFormik } from 'formik';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PaginationButtons, {
	dataPagination,
	PER_COUNT,
} from '../../../../components/PaginationButtons';
import useDarkMode from '../../../../hooks/useDarkMode';
import useSortableData from '../../../../hooks/useSortableData';
import useSelectTable from '../../../../hooks/useSelectTable';
import Icon from '../../../../components/icon/Icon';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import OrderTableRow from '../../../_common/OrderTableRow';
import SubHeader, { SubHeaderLeft } from '../../../../layout/SubHeader/SubHeader';
import Input from '../../../../components/bootstrap/forms/Input';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { LIST_ORDERS } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import { HashLoader } from 'react-spinners';
import DataErrorComponent from '../../../_common/NoDataErrorComponent';

import { downloadImage } from '../../../../helpers/helpers';

const ListOrders = () => {
	const { userData } = useContext(AuthContext);
	const { themeStatus, darkModeStatus } = useDarkMode();
	const [orders, setOrders] = useState<any[]>([]);
	const [filteredData, setFilteredData] = useState<any[]>([]);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState([]);
	const [currentPage, setCurrentPage] = useState<number>(1);
	const [perPage, setPerPage] = useState<number>(PER_COUNT['10']);

	const { items, requestSort, getClassNamesFor } = useSortableData(filteredData);
	const onCurrentPageItems = dataPagination(items, currentPage, perPage);
	const { selectTable, SelectAllCheck } = useSelectTable(onCurrentPageItems);

	const [searchValue, setSearchValue] = useState<string>('');
	const searchOnChange = (e: any) => {
		setSearchValue(e.target.value);

		const searchedData = orders.filter((item) => {
			const query = e.target.value.toLowerCase();

			return (
				item.id.toLowerCase().indexOf(query) >= 0 ||
				item.customer_first_name.toLowerCase().indexOf(query) >= 0 ||
				item.customer_last_name.toLowerCase().indexOf(query) >= 0 ||
				item.customer_phone_number.toLowerCase().indexOf(query) >= 0
				// item.category_title.toLowerCase().indexOf(query) >= 0
			);
		});

		setFilteredData(searchedData);
	};

	useEffect(() => {
		if (!orders) return;
		setFilteredData(orders);
	}, [orders]);

	useEffect(() => {
		if (!userData.pharmacy_id) return;
		let helper = FormDataHelper();
		helper.append('pharmacy_id', userData.pharmacy_id);
		GeneralServices.postRequest(helper, LIST_ORDERS).then(
			(resp) => {
				setOrders(resp.data);
				setLoading(false);
			},
			(err) => {
				setLoading(false);
				setError(err);
			},
		);
	}, [userData.pharmacy_id]);

	return (
		<PageWrapper title={'Orders List'}>
			<SubHeader>
				<SubHeaderLeft>
					<label
						className='border-0 bg-transparent cursor-pointer me-0'
						htmlFor='searchInput'>
						<Icon icon='Search' size='2x' color='primary' />
					</label>
					<Input
						id='searchInput'
						type='search'
						className='border-0 shadow-none bg-transparent'
						placeholder='Search Order by order id, customer name, and customer phone number'
						onChange={searchOnChange}
						value={searchValue}
					/>
				</SubHeaderLeft>
			</SubHeader>
			<Page>
				<Card stretch data-tour='list'>
					<CardHeader>
						<CardLabel>
							<CardTitle>
								Today Attendance
								<small className='ms-2'>
								Present:{' '}
									{selectTable.values.selectedList.length
										? `${selectTable.values.selectedList.length} / `
										: null}
									{filteredData.length}
								</small>
							</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody className='table-responsive' isScrollable>
						<table className='table table-modern table-hover'>
							<thead>
								<tr>
									<th
										scope='col'
										onClick={() => requestSort('order_id')}
										className='cursor-pointer text-decoration-underline'>
										#
										<Icon
											size='lg'
											className={getClassNamesFor('order_id')}
											icon='FilterList'
										/>
									</th>
									<th scope='col'>Image</th>

									<th scope='col'>Name</th>
									<th scope='col'>Check In</th>
									<th scope='col'>Break</th>
									<th scope='col'>Check Out</th>
									<th scope='col'>Total</th>
									<th scope='col'>Status</th>
									<th scope='col'>Shift</th>

									
								</tr>
							</thead>
							<tbody>
								{onCurrentPageItems.length > 0 ? (
									onCurrentPageItems.map((i) => (
										<OrderTableRow
											key={i.id}
											item={i}
											// eslint-disable-next-line react/jsx-props-no-spreading
											{...i}
											selectName='selectedList'
											selectOnChange={selectTable.handleChange}
											selectChecked={selectTable.values.selectedList.includes(
												// @ts-ignore
												i.id.toString(),
											)}
										/>
									))
								) : (
									<div className='empty-table-message'>
										<DataErrorComponent
											loading={loading}
											error={error}
											length={orders.length}
										/>
									</div>
								)}
							</tbody>
						</table>
					</CardBody>
					<PaginationButtons
						data={items}
						label='items'
						setCurrentPage={setCurrentPage}
						currentPage={currentPage}
						perPage={perPage}
						setPerPage={setPerPage}
					/>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default ListOrders;
