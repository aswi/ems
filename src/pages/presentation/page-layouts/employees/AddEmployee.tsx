import React, { useContext, useEffect, useRef, useState } from 'react';
import Button from '../../../../components/bootstrap/Button';
import Card, {
	CardActions,
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../components/bootstrap/forms/Input';
import Page from '../../../../layout/Page/Page';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import { useFormik } from 'formik';
import showNotification from '../../../../components/extras/showNotification';
import Avatar from '../../../../components/Avatar';
import USERS from '../../../../common/data/userDummyData';
import { GoogleMap, useLoadScript } from '@react-google-maps/api';
import Map from './Map';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { ADD_STORE } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import { useAlert } from '../../../../hooks/useAlert';
import ImageUploader from '../../../../components/common/UploadStoreImg';
import regexs from '../../../../constant/Regexs';
import UserImage from './../../../../assets/img/wanna/good-pic.png'




const lib: any[] = ['places'];
const AddStore = () => {
	const { userData } = useContext(AuthContext);
	// const googleMapsApiKey = 'AIzaSyCx2nGsrPaCE2xZULKv2dYXTGFXYc7AfAc'; // CLIENTS
	const googleMapsApiKey = 'AIzaSyCLi0UHrKCw9BX8IghFd0A7TKlAcUj3I9Q'; // DEV

	const [AlertMessage, isVisible, getProps] = useAlert();
	const { isLoaded } = useLoadScript({
		googleMapsApiKey: googleMapsApiKey,
		libraries: lib,
	});

	const avatarRef = React.useRef<any>(null);
	const testRef = React.useRef<HTMLInputElement>(null);
	const inputFileRef = React.useRef<HTMLInputElement>(null);
	const [selectedFile, setSelectedFile] = useState<any>(undefined);
	const [isMapValueEmpty, setIsMapValueEmpty] = useState<boolean>(false);
	const [preview, setPreview] = useState<any>(undefined);

	const validate = (values: {
		name: string;
		office_email: string;
		phone_number: number | string;
		employee_address: string;
		joining_date: string;
		salary: number | string;
		storeImage: string;
		office_email_password:number | string;
		personal_email:string;
		employee_documents:any
		department:string

	}) => {
		const errors: {
			name?: string;
			office_email?: string;
			phone_number?: number | string;
			employee_address?: string;
			joining_date?: string;
			salary?: number | string;
			storeImage?: string;
			office_email_password?:number | string;
			 personal_email?:string
			employee_documents?:any
			department?:string
		} = {};
		if (!values.name) {
			errors.name = 'Required';
		} else if (!regexs.nameRegex.regex.test(values.name)) {
			errors.name = regexs.nameRegex.errMessage;
		}

		if (!values.office_email) {
			errors.office_email = 'Required';
		} else if (!regexs.emailRegex.regex.test(values.office_email)) {
			errors.office_email = regexs.emailRegex.errMessage;
		}

		if (!values.phone_number) {
			errors.phone_number = 'Required';
		} else if (!regexs.phoneRegex.regex.test(String(values.phone_number))) {
			errors.phone_number = regexs.phoneRegex.errMessage;
		}

		if (!values.employee_address) {
			errors.employee_address = 'Required';
		} else if (!regexs.addressRegex.regex.test(values.employee_address)) {
			errors.employee_address= regexs.addressRegex.errMessage;
		}

		if (!values.joining_date) {
			errors.joining_date = 'Required';
		}

		
		if (!values.department) {
			errors.department = 'Required';
		}
		
		if (!values.salary) {
			errors.salary = 'Required';
		}
		if  (!regexs.passwordRegex.regex.test(String(values.office_email_password))) {
			errors.office_email_password = regexs.passwordRegex.errMessage;
		}
		return errors;
	};

	const formik = useFormik({
		initialValues: {
			name: '',
			office_email: '',
			employee_address: '',
			phone_number: '',
			joining_date: '',
			salary: '',
			storeImage: '',
			office_email_password:'',
			personal_email:'',
			department:'',
			employee_documents:'',
			

		},
		validate,
		onSubmit: (values, { setSubmitting }) => {
			setSubmitting(true);
			let formData = FormDataHelper();
			formData.append('pharmacy_id', userData.pharmacy_id!);
			formData.append('name', values.name);
			formData.append('office_email', values.office_email);
			formData.append('phone_number', values.phone_number);
			formData.append('joining_date', values.joining_date);
			formData.append('salary', values.salary);
			formData.append('address', values.employee_address);
			console.log(values)

			if (values.storeImage != null) {
				formData.append('image', values.storeImage);
			}

			GeneralServices.postRequest(formData, ADD_STORE).then(
				(resp) => {
					if (inputFileRef.current) inputFileRef.current.value = '';
					getProps({ variant: 'success', message: resp.message });
					formik.resetForm();
					setSelectedFile(undefined);
					setIsMapValueEmpty(true);
					setSubmitting(false);
				},
				(err) => {
					getProps({ variant: 'danger', message: err }); // eslint-disable-line react-hooks/exhaustive-deps
				},
			);
		},
	});

	const updateLatLngValues = (lat: number, lng: number) => {
		formik.setFieldValue('store_lat', lat.toString());
		formik.setFieldValue('store_long', lng.toExponential());
		testRef.current?.focus();
		setTimeout(function () {
			testRef.current?.blur();
		}, 1000);

		// testRef.current?.blur();
	};

	useEffect(() => {
		if (!selectedFile) {
			setPreview(undefined);
			return;
		}

		const objectUrl = URL.createObjectURL(selectedFile);
		setPreview(objectUrl);

		return () => URL.revokeObjectURL(objectUrl);
	}, [selectedFile]);

	return (
		<PageWrapper>
			
			<Page>
				{/* label */}
				<div className='col-lg-12 h-100'>
					<form className='row g-4' noValidate onSubmit={formik.handleSubmit}>
						<ImageUploader
							defaultImage={preview || UserImage}
							onImageSelected={(file) => {
								formik.setFieldValue('storeImage', file);
							}}
						/>

						<Card>
							<CardHeader>
								<CardLabel icon='FormatSize' iconColor='warning'>
									<CardTitle>Employee</CardTitle>
									<CardSubTitle>Employee Information</CardSubTitle>
								</CardLabel>
							</CardHeader>
							<CardBody>
								{isVisible ? <AlertMessage /> : ''}
								{/* <Alert isDismissible>A simple alert—check it out!</Alert> */}
								<div className='row g-4'>
									<FormGroup className='col-6' id='name' label='Employee Name'>
										<Input
											placeholder='Employee Name'
											onChange={formik.handleChange}
											value={formik.values.name}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.name}
											invalidFeedback={formik.errors.name}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup
										className='col-6'
										id='phone_number'
										label='Phone Number'>
										<Input
											type='text'
											placeholder='Enter Phone Number'
											required
											onChange={formik.handleChange}
											value={formik.values.phone_number}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.phone_number}
											invalidFeedback={formik.errors.phone_number}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									
									<FormGroup
										className='col-6'
										id='office_email'
										label='Office Email'>
										<Input
											type='text'
											placeholder='Enter Office Email'
											required
											onChange={formik.handleChange}
											value={formik.values.office_email}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.office_email}
											invalidFeedback={formik.errors.office_email}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup
										className='col-6'
										id='office_email_password'
										label='Office Email Password'>
										<Input
											type='text'
											placeholder='Enter Office Email Password'
											required
											onChange={formik.handleChange}
											value={formik.values.office_email_password}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.office_email_password}
											invalidFeedback={formik.errors.office_email_password}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup
										className='col-6'
										id='personal_email'
										label='Personal Email'>
										<Input
											type='text'
											placeholder='Enter Personal Email'
											required
											onChange={formik.handleChange}
											value={formik.values.personal_email}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.personal_email}
											invalidFeedback={formik.errors.personal_email}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup
										className='col-6'
										id='department'
										label='Department'>
										<Input
											type='text'
											placeholder='Enter Department'
											required
											onChange={formik.handleChange}
											value={formik.values.department}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.department}
											invalidFeedback={formik.errors.department}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup
										className='col-6'
										id='employee_address'
										label='Employee Address'>
										<Input
											type='text'
											placeholder='Enter Employee Address'
											ref={testRef}
											onChange={formik.handleChange}
											value={formik.values.employee_address}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.employee_address}
											invalidFeedback={formik.errors.employee_address}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup className='col-3' id='joining_date' label='Joining Date'>
										<Input
											type='date'
											placeholder='Enter Joining Date'
											// ref={latRef}
											onChange={formik.handleChange}
											value={formik.values.joining_date}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.joining_date}
											invalidFeedback={formik.errors.joining_date}
											// disabled
											// readOnly
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup className='col-3' id='salary' label='Basic Salary'>
										<Input
											type='text'
											placeholder='Enter Basic Salary'
											// ref={lngRef}
											onChange={formik.handleChange}
											value={formik.values.salary}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.salary}
											invalidFeedback={formik.errors.salary}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup className='col-12' id='employee_documents' label='Upload Documents'>
										<Input
											type='file'
											placeholder='Choose Documents'
											// ref={lngRef}
											onChange={formik.handleChange}
											value={formik.values.employee_documents}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.employee_documents}
											invalidFeedback={formik.errors.employee_documents}
											validFeedback='Looks good!'
										/>
									</FormGroup>

									<div className='col'>
										<Button
											type='submit'
											color='info'
											icon='Save'
											isDisable={!formik.isValid}>
											Save
										</Button>
									</div>
								</div>
							</CardBody>
						</Card>
					</form>
					{/* <Card>
						<CardBody>
							{isLoaded && (
								<Map
									updateLatLngValues={updateLatLngValues}
									initialValues={{ lat: 33.8547, lng: 35.8623 }}
									isValueEmpty={isMapValueEmpty}></Map>
							)}
						</CardBody>
					</Card> */}
				</div>
			</Page>
		</PageWrapper>
	);
};

export default AddStore;
