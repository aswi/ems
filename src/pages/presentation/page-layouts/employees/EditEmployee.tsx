import React, { useContext, useEffect } from 'react';
import { useState } from 'react';
import Button from '../../../../components/bootstrap/Button';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../components/bootstrap/forms/Input';
import Page from '../../../../layout/Page/Page';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import { useFormik } from 'formik';
import showNotification from '../../../../components/extras/showNotification';
import USERS from '../../../../common/data/userDummyData';
import Avatar from '../../../../components/Avatar';
import { useLoadScript } from '@react-google-maps/api';
import Map from './Map';
import { useParams } from 'react-router-dom';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { DELETE_IMAGE, EDIT_STORE, LIST_STORES } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import Swal from 'sweetalert2';
import { useAlert } from '../../../../hooks/useAlert';
import ImageUploader from '../../../../components/common/UploadStoreImg';

export interface IStore {
	name: string;
	registration_number: string;
	phone_number: string;
	address: string;
	latitude: string;
	longitude: string;
	image: string;
	is_default_image: string;
}

const lib: any[] = ['places'];
const EditStore = () => {
	let { id } = useParams();
	const { userData } = useContext(AuthContext);
	const [AlertMessage, isVisible, getProps] = useAlert();
	const latRef = React.useRef<HTMLInputElement>(null);
	const lngRef = React.useRef<HTMLInputElement>(null);
	const [selectedFile, setSelectedFile] = useState<any>(undefined);
	const [isMapValueEmpty, setIsMapValueEmpty] = useState<boolean>(false);

	const [preview, setPreview] = useState<any>(undefined);
	const avatarRef = React.useRef<any>(null);
	const inputFileRef = React.useRef<HTMLInputElement>(null);
	const [store, setStore] = useState<IStore>({
		name: '',
		registration_number: '',
		address: '',
		phone_number: '',
		latitude: '',
		longitude: '',
		image: '',
		is_default_image: '',
	});

	const { isLoaded } = useLoadScript({
		googleMapsApiKey: 'AIzaSyCLi0UHrKCw9BX8IghFd0A7TKlAcUj3I9Q',
		libraries: lib,
	});
	const validate = (values: {
		name: string;
		registration_number: string;
		phone_number: string;
		address: string;
		latitude: string;
		longitude: string;
		storeImage: string;
	}) => {
		const errors: {
			name?: string;
			registration_number?: string;
			phone_number?: string;
			address?: string;
			latitude?: string;
			longitude?: string;
		} = {};
		if (!values.name) {
			errors.name = 'Required';
		} else if (values.name.length < 6) {
			errors.name = 'Must be 5 characters or more';
		}

		if (!values.registration_number) {
			errors.registration_number = 'Required';
		}

		if (!values.phone_number) {
			errors.phone_number = 'Required';
		}

		if (!values.address) {
			errors.address = 'Required';
		}

		if (!values.latitude) {
			errors.latitude = 'Required';
		}

		if (!values.longitude) {
			errors.longitude = 'Required';
		}
		return errors;
	};
 
	const formik = useFormik({
		initialValues: {
			name: store.name,
			registration_number: store.registration_number,
			address: store.address,
			phone_number: store.phone_number,
			latitude: store.latitude,
			longitude: store.longitude,
			storeImage: '',
		},
		validate,
		enableReinitialize: true,
		onSubmit: (values , { setSubmitting }) => {
			let formData = FormDataHelper();
			formData.append('pharmacy_id', userData.pharmacy_id!);
			formData.append('name', values.name);
			formData.append('registration_number', values.registration_number);
			formData.append('phone_number', values.phone_number);
			formData.append('latitude', values.latitude);
			formData.append('longitude', values.longitude);
			formData.append('address', values.address);

			formData.append('store_id', id!);

			if (values.storeImage != null) {
				formData.append('image', values.storeImage);
			}

			GeneralServices.postRequest(formData, EDIT_STORE).then(
				(resp) => {
					getStoreData();
					getProps({ variant: 'success', message: resp.message });
					// showNotification(`${process.env.REACT_APP_SITE_NAME}`, resp.message, 'success');
					setIsMapValueEmpty(true)
					setSubmitting(false);
				},
				(err) => {
					getProps({ variant: 'danger', message: err.message });
					// showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
				},
			);
		},
	});

	const updateLatLngValues = (lat: number, lng: number) => {
		formik.setFieldValue('latitude', lat.toString());
		formik.setFieldValue('longitude', lng.toString());
	};

	function getStoreData() {
		if (id) {
			let formData = FormDataHelper();
			formData.append('store_id', id);
			GeneralServices.postRequest(formData, LIST_STORES).then(
				(resp) => {
					let storeData: IStore = resp.data[0];
					setStore(storeData);
				},
				(err) => {
					console.log('failed to load store');
				},
			);
		}
	}

	const handleDeleteImage = () => {
		Swal.fire({
			title: 'MedSouk',
			text: 'Are you sure you want to delete the store image?',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.isConfirmed) {
				if (!id) return;
				let helper = FormDataHelper();
				helper.append('delete_image_type', 'store');
				helper.append('store_id', id);

				GeneralServices.postRequest(helper, DELETE_IMAGE).then(
					(resp) => {
						getStoreData();
						getProps({ variant: 'success', message: resp.message });
					},
					(err) => {
						getProps({ variant: 'danger', message: err });
					},
				);
			}
		});
	};

	useEffect(() => {
				if (store.is_default_image==='N') {
					setPreview(store.image);
		  } else if (selectedFile) {
					const objectUrl = URL.createObjectURL(selectedFile);
			setPreview(objectUrl);
			return () => URL.revokeObjectURL(objectUrl);
		  } else {
		
			setPreview(undefined);
		  }
	}, [store.is_default_image,store.image,selectedFile]);

	useEffect(() => {
		getStoreData();
	}, [id]); // eslint-disable-line react-hooks/exhaustive-deps

	if (!store.name) return <></>;
	console.log({preview})
let test = store.image

	return (
		<PageWrapper>
			<Page>
				{/* label */}
				<div className='col-lg-12 h-100'>
					{/* <Card>
						<CardHeader>
							<CardLabel icon='FormatSize' iconColor='warning'>
								<CardTitle>Stores</CardTitle>
								<CardSubTitle>Upload Store Image</CardSubTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<div className='col-12'> */}
								{/* <div className='row g-4 align-items-center'>
									<div className='col-lg-auto'>
										<Avatar
											// srcSet={USERS.JOHN.srcSet}
											ref={avatarRef}
											src={  preview || USERS.JOHN.src }
											color={USERS.JOHN.color}
											rounded={3}
										/>
									</div>
									<div className='col-lg'>
										<div className='row g-4'>
											<div className='col-auto'>
											<Input
														type='file'
														id='storeImage'
														name='storeImage'
														autoComplete='photo'
														ref={inputFileRef}
														accept='image/png, image/jpeg'
														onChange={(event: any) => {
															formik.setFieldValue(
																'storeImage',
																event.currentTarget.files,
															);

															if (
																!event.target.files ||
																event.target.files.length === 0
															) {
																setSelectedFile(undefined);
																return;
															}

															// I've kept this example simple by using the first image instead of multiple
															setSelectedFile(event.target.files[0]);
														}}
													/>
											</div>
											{store.is_default_image == 'N' && (
												<div className='col-auto'>
													<Button
														color='dark'
														isLight
														icon='Delete'
														onClick={handleDeleteImage}>
														Delete Avatar
													</Button>
												</div>
											)}
											<div className='col-12'>
												<p className='lead text-muted'>
													Store Avatar helps your customers get to know
													you.
												</p>
											</div>
										</div>
									</div>
								</div> */}
									<ImageUploader
  defaultImage={store.image || USERS.JOHN.src}
  onImageSelected={(file) => {
    formik.setFieldValue('storeImage', file);
  }}
  allowDelete={store.is_default_image === 'N'}
  onDeleteImage={handleDeleteImage}
/>
							{/* </div>
						
						</CardBody>
					</Card> */}

					<Card>
						<CardHeader>
							<CardLabel icon='FormatSize' iconColor='warning'>
								<CardTitle>Stores</CardTitle>
								<CardSubTitle>Edit Store Information</CardSubTitle>
							</CardLabel>
						</CardHeader>

						<CardBody>
							{isVisible ? <AlertMessage /> : ''}
							<form className='row g-4' noValidate onSubmit={formik.handleSubmit}>
								<FormGroup className='col-12' id='name' label='Name'>
									<Input
										placeholder='Store Name'
										onChange={formik.handleChange}
										value={formik.values.name}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.name}
										invalidFeedback={formik.errors.name}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup
									className='col-6'
									id='registration_number'
									label='Registration Number'>
									<Input
										type='text'
										placeholder='Enter Registration Number'
										required
										onChange={formik.handleChange}
										value={formik.values.registration_number}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.registration_number}
										invalidFeedback={formik.errors.registration_number}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup className='col-6' id='phone_number' label='Phone Number'>
									<Input
										type='text'
										placeholder='Enter Phone Number'
										required
										onChange={formik.handleChange}
										value={formik.values.phone_number}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.phone_number}
										invalidFeedback={formik.errors.phone_number}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup className='col-6' id='address' label='Store Address'>
									<Input
										type='text'
										placeholder='Enter Store Address'
										required
										onChange={formik.handleChange}
										value={formik.values.address}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.address}
										invalidFeedback={formik.errors.address}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup className='col-3' id='latitude' label='Store Lat'>
									<Input
										type='text'
										placeholder='Enter Store Lat'
										ref={latRef}
										onChange={formik.handleChange}
										value={formik.values.latitude}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.latitude}
										invalidFeedback={formik.errors.latitude}
										validFeedback='Looks good!'
										disabled
									/>
								</FormGroup>
								<FormGroup className='col-3' id='longitude' label='Store Long'>
									<Input
										type='text'
										placeholder='Enter Store Long'
										ref={lngRef}
										onChange={formik.handleChange}
										value={formik.values.longitude}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.longitude}
										invalidFeedback={formik.errors.longitude}
										validFeedback='Looks good!'
										disabled
									/>
								</FormGroup>
								<div className='col'>
									<Button
										type='submit'
										color='info'
										icon='Save'
										isDisable={!formik.isValid}>
										Save
									</Button>
								</div>
							</form>
						</CardBody>
					</Card>
					<Card>
						<CardBody>
							{isLoaded && (
								<Map
									updateLatLngValues={updateLatLngValues}
									initialValues={{
										lat: Number(store.latitude),
										lng: Number(store.longitude),
									}}
									isValueEmpty={isMapValueEmpty}
									// setIsMapValueEmpty={setIsMapValueEmpty}

									></Map>
							)}
						</CardBody>
					</Card>
				</div>
			</Page>
		</PageWrapper>
	);
};

export default EditStore;
