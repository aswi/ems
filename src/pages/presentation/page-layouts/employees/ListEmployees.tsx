import React, { useContext, useEffect, useState } from 'react';
import { useFormik } from 'formik';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PaginationButtons, {
	dataPagination,
	PER_COUNT,
} from '../../../../components/PaginationButtons';
import useDarkMode from '../../../../hooks/useDarkMode';
import useSortableData from '../../../../hooks/useSortableData';
import useSelectTable from '../../../../hooks/useSelectTable';
import Icon from '../../../../components/icon/Icon';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import StoreTableRow from '../../../_common/StoreTableRow';
import { GeneralServices } from '../../../../services/General.Services';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { LIST_STORES } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import { HashLoader } from 'react-spinners';
// import '..//categories.css';
import DataErrorComponent from '../../../_common/NoDataErrorComponent';

const ListStores = () => {
	const [stores, setStores] = useState<any[]>([]);
	const [loading, setLoading] = useState(true);
	const [filteredData, setFilteredData] = useState<any[]>([]);
	const [error, setError] = useState([]);

	const [currentPage, setCurrentPage] = useState<number>(1);
	const [perPage, setPerPage] = useState<number>(PER_COUNT['10']);

	const { items, requestSort, getClassNamesFor } = useSortableData(filteredData);
	const onCurrentPageItems = dataPagination(items, currentPage, perPage);
	const { selectTable, SelectAllCheck } = useSelectTable(onCurrentPageItems);
	const { userData } = useContext(AuthContext);

	useEffect(() => {
		let formData = FormDataHelper();
		formData.append('pharmacy_id', userData.pharmacy_id!);

		// setLoading(true);
		GeneralServices.postRequest(formData, LIST_STORES).then(
			(resp) => {
				setStores(resp.data);
				setLoading(false);
			},
			(err) => {
				setLoading(false);
				setError(err);
			},
		);
	}, [userData.pharmacy_id]);

	useEffect(() => {
		setFilteredData(stores);

		// setStores([])
	}, [stores]);

	return (
		<PageWrapper title={'List Stores'}>
			<Page>
				<Card stretch data-tour='list'>
					<CardHeader>
						<CardLabel>
							<CardTitle>
								Employees List
								<small className='ms-2'>
									Total:{' '}
									{selectTable.values.selectedList.length
										? `${selectTable.values.selectedList.length} / `
										: null}
									{filteredData.length}
								</small>
							</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody className='table-responsive' isScrollable>
						<table className='table table-modern table-hover'>
							<thead>
								<tr>
									<th
										scope='col'
										onClick={() => requestSort('id')}
										className='cursor-pointer text-decoration-underline'>
										#
										<Icon
											size='lg'
											className={getClassNamesFor('id')}
											icon='FilterList'
										/>
									</th>

									<th scope='col'>Image</th>
									<th scope='col'>Name</th>
									<th scope='col'>Office Email</th>
									<th scope='col'>Phone Number</th>
									<th scope='col'>Employee Address</th>
									<th scope='col'>Joining Date</th>
									<th scope='col'>salary</th>
									<th scope='col' className='text-end'>
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								{onCurrentPageItems.length > 0 ? (
									onCurrentPageItems.map((i) => (
										<StoreTableRow
											key={i.id}
											// eslint-disable-next-line react/jsx-props-no-spreading
											{...i}
											selectName='selectedList'
											selectOnChange={selectTable.handleChange}
											selectChecked={selectTable.values.selectedList.includes(
												// @ts-ignore
												i.id.toString(),
											)}
										/>
									))
								) : (
									<div className='empty-table-message'>
										<DataErrorComponent
											loading={loading}
											error={error}
											length={stores.length}
										/>
									</div>
								)}
							</tbody>
						</table>
					</CardBody>
					<PaginationButtons
						data={items}
						label='items'
						setCurrentPage={setCurrentPage}
						currentPage={currentPage}
						perPage={perPage}
						setPerPage={setPerPage}
					/>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default ListStores;
