import {
	Combobox,
	ComboboxInput,
	ComboboxList,
	ComboboxOption,
	ComboboxPopover,
} from '@reach/combobox';
import '@reach/combobox/styles.css';
import { GoogleMap, MarkerF } from '@react-google-maps/api';


import React, { FC, FunctionComponent, memo, useEffect, useMemo, useState } from 'react';
import usePlacesAutocomplete, { getGeocode, getLatLng } from 'use-places-autocomplete';
import useDarkMode from '../../../../hooks/useDarkMode';

interface IMapProps {
	updateLatLngValues: any;
	initialValues?: any;
	isValueEmpty: boolean;
}

const Map: FC<IMapProps> = ({ updateLatLngValues, initialValues = null, isValueEmpty }) => {
	const center = useMemo(() => initialValues, [initialValues]);
	const [selected, setSelected] = useState<any>(null);
	
	useEffect(() => {
		if (!selected || !selected.lat || !selected.lng) return;
		updateLatLngValues(selected.lat, selected.lng);
	}, [selected]); // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<>
			<div className='places-container'>
				<PlacesAutocomplete
					setSelected={setSelected}
					isValueEmpty={isValueEmpty}></PlacesAutocomplete>
			</div>
			<GoogleMap
				zoom={10}
				center={selected || center}
				// mapContainerClassName='map-container'
				mapContainerStyle={{ width: '100%', height: '400px' }}>
				<MarkerF
					position={selected || center}
					draggable
					onDragEnd={(e) => {
						setSelected({ lat: e.latLng?.lat(), lng: e.latLng?.lng() });
					}}></MarkerF>
			</GoogleMap>
		</>
	);
};

interface IPlacesAutocompleteProps {
	setSelected: React.Dispatch<React.SetStateAction<any>>;
	isValueEmpty: boolean;
}

const PlacesAutocomplete: FunctionComponent<IPlacesAutocompleteProps> = ({
	setSelected,
	isValueEmpty,
}) => {
	const {
		ready,
		value,
		setValue,
		suggestions: { status, data },
		clearSuggestions,
	} = usePlacesAutocomplete();
	const { darkModeStatus} = useDarkMode()
 
	const handleSelect = async (address: string) => {
		setValue(address, false);
		clearSuggestions();

		const results = await getGeocode({ address });
		const { lat, lng } = await getLatLng(results[0]);
		setSelected({ lat, lng });
	};
	useEffect(() => {
		if (isValueEmpty) {
			setValue('');
		}
	}, [isValueEmpty]); // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<Combobox onSelect={handleSelect}>
			<ComboboxInput
				value={value}
				onChange={(e) => setValue(e.target.value)}
				disabled={!ready}
				placeholder='search ...'
				className='combobox-input'
			/>
			<ComboboxPopover>
				<ComboboxList  style={{
            backgroundColor: darkModeStatus ? '#242f3e' : 'white', // Change the background color based on darkModeStatus
            color: darkModeStatus ? '#fff' : 'black', // Change the text color based on darkModeStatus
        
          }}>
					{status === 'OK' &&
						data.map(({ place_id, description }) => (
							<ComboboxOption key={place_id} value={description} />
						))}
				</ComboboxList>
			</ComboboxPopover>
		</Combobox>
	);
};

export default memo(Map);
