import React, { useContext, useEffect, useState } from 'react';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../components/bootstrap/forms/Input';
import Textarea from '../../../../components/bootstrap/forms/Textarea';
import Button from '../../../../components/bootstrap/Button';
import { useFormik } from 'formik';
import showNotification from '../../../../components/extras/showNotification';
import Select from '../../../../components/bootstrap/forms/Select';
import { GeneralServices } from '../../../../services/General.Services';
import { FormDataHelper } from '../../../../services/FormDataHelper';
// import { ADD_PRODUCT, LIST_CATEGORIES } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import { useAlert } from '../../../../hooks/useAlert';
import regexs from '../../../../constant/Regexs';
import { LEAVE_OPTIONS, TIME_OPTIONS } from '../../../../constant/options';

const AddProduct = () => {
	const { userData } = useContext(AuthContext);
	const [AlertMessage, isVisible, getProps] = useAlert();

	const inputFileRef = React.useRef<HTMLInputElement>(null);
	const positiveNumberRegex = /^[^-]*$/;
	
	const validate = (values: {
		name: string;
		from: string;
		to: string;
		phone_number:string;
		total_leaves: string;
		short_description?: string;
		long_description: string;
		leave_type: string;
		formFile: FileList | null;
	}) => {
		const errors: {
			name?: string;
			from?: string;
			to?: string;
			phone_number?:string
			total_leaves?: string;
			short_description?: string;
			long_description?: string;
			leave_type?: string;
			formFile?: string;
		} = {};

		if (!values.name) {
			errors.name = 'Required';
		} else if (!regexs.nameRegex.regex.test(values.name)) {
			errors.name = regexs.nameRegex.errMessage;
		}

		if (!values.from) {
			errors.from = 'Required';
		} else if (values.from !== '' && Number(values.from) <= 0) {
			errors.from = 'Please enter start date';
		}

		if (!values.to) {
			errors.to = 'Required';
		}
		console.log(typeof(Number(values.total_leaves)))
		if (!values.total_leaves ) {
			errors.total_leaves = 'Unable to apply for leaves.';
		}else
		 if(Number(values.total_leaves) <= 0){
			errors.total_leaves = 'Unable to apply for leaves.';
		}
		
		if (!values.leave_type) {
			errors.leave_type = 'Required';
		}

		if (values.formFile) {
			const allowedExtensions = ['png', 'jpg', 'jpeg'];

			let files: FileList = values.formFile;
			for (let i = 0; i < files.length; i++) {
				const extension: string | undefined = files[i].name.split('.').pop();
				if (!allowedExtensions.includes(extension!)) {
					errors.formFile = 'Unsupported File Format, Supported file formates are: ';
				}
			}
		}

		return errors;
	};

	const formik = useFormik({
		initialValues: {
			name: '',
			from: '',
			to: '',
			total_leaves: '',
			phone_number:'',
			short_description: '',
			long_description: '',
			leave_type: '',
			formFile: null,
		},
		validate,
		onSubmit: (values) => {
			let formHelper = FormDataHelper();
			formHelper.append('leave_type', values.leave_type);
			formHelper.append('title', values.name);
			formHelper.append('short_description', values.short_description!);
			formHelper.append('start_from', values.from);
			formHelper.append('end_to', values.to);
			formHelper.append('total_leaves', values.total_leaves);
			formHelper.append('phone_number', values.phone_number);


			

			if (values.formFile != null) {
				for (let imageFile of values.formFile as any) {
					formHelper.append('attachments[]', imageFile);
				}
			}
			// GeneralServices.postRequest(formHelper, ADD_PRODUCT).then(
			// 	(resp) => {
			// 		formik.resetForm();
			// 		getProps({ variant: 'success', message: resp.message });
			// 		console.log(resp.message);
			// 		if (inputFileRef.current) inputFileRef.current.value = '';
			// 		// showNotification(`${process.env.REACT_APP_SITE_NAME}`, resp.message, 'success');
			// 	},
			// 	(err) => {
			// 		getProps({ variant: 'danger', message: err });
			// 		// showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
			// 	},
			// );
		},
	});
	const calculateTotalDays = () => {
		if (formik.values.from && formik.values.to) {
			const fromDate = new Date(formik.values.from);
			const toDate = new Date(formik.values.to);
			let totalDays = 0;
	
			while (fromDate <= toDate) {
				const day = fromDate.getDay();
				if (day !== 0 && day !== 6) {
					totalDays++;
				}
				fromDate.setDate(fromDate.getDate() + 1);
			}
	
			formik.setFieldValue('total_leaves', totalDays);
	
			if (totalDays === 0) {
				formik.setFieldError('total_leaves', 'Leave duration must be at least one day.');
			} else {
				formik.setFieldError('total_leaves', ''); // Clear the error if totalDays > 0
			}
		} else {
			formik.setFieldValue('total_leaves', 0);
		}
	};
	
	const [categories, setCategories] = useState<any>([]);
	const [categoriesList, setCategoriesList] = useState([]);
	useEffect(() => {
		// let arr: any = [];
		// categories?.forEach((cat: any) => {
		// 	arr.push({ value: cat.id, text: cat.title });
		// });

		// setCategoriesList(arr);
		calculateTotalDays();
	}, [formik.values.from, formik.values.to]); // eslint-disable-line react-hooks/exhaustive-deps

	// useEffect(() => {
	// 	let formData = FormDataHelper();
	// 	GeneralServices.postRequest(formData, LIST_CATEGORIES).then(
	// 		(resp) => {
	// 			setCategories(resp?.data);
	// 		},
	// 		(err) => {
	// 			getProps({ variant: 'danger', message: err });

	// 			// showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
	// 		},
	// 	);
	// }, []); // eslint-disable-line react-hooks/exhaustive-deps
	// const handleChange = () => {};

	return (
		<PageWrapper>
			<Page>
				<div className='col-lg-12 h-100'>
					<Card stretch>
						<CardHeader>
							<CardLabel icon='FormatSize' iconColor='warning'>
								<CardTitle>Leaves</CardTitle>
								<CardSubTitle>Add Leave</CardSubTitle>
							</CardLabel>
						</CardHeader>

						<CardBody>
							{isVisible && <AlertMessage />}
							<form className='row g-4' noValidate onSubmit={formik.handleSubmit}>
								{/* <FormGroup className='col-6' id='name' label='Name'>
									<Input
										placeholder='Your Name'
										onChange={formik.handleChange}
										value={formik.values.name}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.name}
										invalidFeedback={formik.errors.name}
										disabled
										readOnly
										validFeedback='Looks good!'
									/>
								</FormGroup> */}
								<FormGroup className='col-4' id='name' label='Leave Balance'>
									<Input
										placeholder='Your Leave Balance'
										onChange={formik.handleChange}
										value={formik.values.name}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.name}
										invalidFeedback={formik.errors.name}
										disabled
										readOnly
										validFeedback='Looks good!'
									/>
								</FormGroup>

								
								<FormGroup
									id='leave_type'
									label='Please Select Leave Type'
									className='col-4'>
									<Select
										ariaLabel='Leave Type'
										placeholder='---Select Leave Type---'
										onChange={formik.handleChange}
										value={formik.values.leave_type}
										list={LEAVE_OPTIONS}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.leave_type}
										invalidFeedback={formik.errors.leave_type}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup
									id='leave_type'
									label='Please Select Leave Type'
									className='col-4'>
									<Select
										ariaLabel='Time'
										placeholder='---Select Time---'
										onChange={formik.handleChange}
										value={formik.values.leave_type}
										list={TIME_OPTIONS}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.leave_type}
										invalidFeedback={formik.errors.leave_type}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								
								<FormGroup className='col-4' id='from' label='From Date'>
									<Input
										type='date'
										placeholder='started From'
										onChange={formik.handleChange}
										value={formik.values.from}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.from}
										invalidFeedback={formik.errors.from}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup className='col-4' id='to' label='To Date'>
									<Input
										type='date'
										placeholder='Enter Leave Name'
										onChange={formik.handleChange}
										value={formik.values.to}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.to}
										invalidFeedback={formik.errors.to}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup className='col-4' id='total' label='Total No Of Days'>
									<Input
										placeholder='Total Leaves'
										onChange={formik.handleChange}
										value={formik.values.total_leaves}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.total_leaves}
										invalidFeedback={formik.errors.total_leaves}
										disabled
										readOnly
										validFeedback='Looks good!'
									/>
								</FormGroup>


								<FormGroup
									className='col-12'
									id='short_description'
									label='Reason'>
									<Textarea
										placeholder='Enter Reason'
										onChange={formik.handleChange}
										value={formik.values.short_description}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.short_description}
										invalidFeedback={formik.errors.short_description}
										validFeedback='Looks good!'></Textarea>
								</FormGroup>

								<div className='col mt-5'>
									<Button
										type='submit'
										color='info'
										icon='Save'
										isDisable={!formik.isValid}>
										Apply Leave
									</Button>
								</div>
							</form>
						</CardBody>
					</Card>
				</div>
			</Page>
		</PageWrapper>
	);
};

export default AddProduct;
