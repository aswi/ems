import React, { useContext, useState, useEffect, useRef, useCallback } from 'react';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../components/bootstrap/forms/Input';
import Textarea from '../../../../components/bootstrap/forms/Textarea';
import Button from '../../../../components/bootstrap/Button';
import { useFormik } from 'formik';
import showNotification from '../../../../components/extras/showNotification';
import Select from '../../../../components/bootstrap/forms/Select';
import { useParams } from 'react-router-dom';
import AuthContext from '../../../../contexts/authContext';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import {
	DELETE_IMAGE,
	EDIT_PRODUCT,
	LIST_CATEGORIES,
	LIST_PRODUCTS,
} from '../../../../services/axiousURL';
import { CardFooter } from '../../../../components/bootstrap/Card';
import Avatar from '../../../../components/Avatar';
import Swal from 'sweetalert2';
import { useAlert } from '../../../../hooks/useAlert';

const EditProduct = () => {
	let { id } = useParams();
	const [AlertMessage, isVisible, getProps] = useAlert();
	const inputFileRef = React.useRef<HTMLInputElement>(null);
	const { userData } = useContext(AuthContext);
	const [categories, setCategories] = useState<any>([]);
	const [categoriesList, setCategoriesList] = useState([]);
	const [product, setProduct] = useState<any>({
		title: '',
		price: '',
		category_id: 0,
		short_description: '',
		long_description: '',
		images_data: [],
	});

	const CURRENCY_OPTIONS = [
		{ value: 'USD', text: 'USD' },
		{ value: 'LBP', text: 'LBP' },
	];

	const validate = (values: {
		name: string;
		product_price: string;
		currency_id: string;
		short_description?: string;
		long_description: string;
		category_id: string;
		formFile: string;
	}) => {
		const errors: {
			name?: string;
			product_price?: string;
			currency_id?: string;
			short_description?: string;
			long_description?: string;
			category_id?: string;
		} = {};
		if (!values.name) {
			errors.name = 'Required';
		}

		if (!values.product_price) {
			errors.product_price = 'Required';
		}

		if (!values.category_id) {
			errors.category_id = 'Required';
		}

		return errors;
	};

	const formik = useFormik({
		initialValues: {
			name: product.title,
			product_price: product.price,
			currency_id: product.currency,
			short_description: product.short_description,
			long_description: product.long_description,
			category_id: product.category_id,
			formFile: '',
		},
		enableReinitialize: true,
		validate,
		onSubmit: (values) => {
			if (!id) return;

			let formHelper = FormDataHelper();
			formHelper.append('category_id', values.category_id);
			formHelper.append('pharmacy_id', userData.pharmacy_id!);
			formHelper.append('title', values.name);
			formHelper.append('short_description', values.short_description!);
			formHelper.append('long_description', values.long_description);
			formHelper.append('price', values.product_price);
			formHelper.append('currency', values.currency_id);
			formHelper.append('product_id', id);

			if (values.formFile != null) {
				for (let imageFile of values.formFile as any) {
					formHelper.append('attachments[]', imageFile);
				}
			}

			GeneralServices.postRequest(formHelper, EDIT_PRODUCT).then(
				(resp) => {
					if (inputFileRef.current) inputFileRef.current.value = '';
					fetchProductDetail();
					getProps({ variant: 'success', message: resp.message });
					// showNotification(`${process.env.REACT_APP_SITE_NAME}`, resp.message, 'success');
				},
				(err) => {
					getProps({ variant: 'danger', message: err });
					// showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
				},
			);
		},
	});

	const fetchProductDetail = useCallback(() => {
		if (!id) return;
		let helper = FormDataHelper();
		helper.append('product_id', id);
		GeneralServices.postRequest(helper, LIST_PRODUCTS).then(
			(resp) => {
				setProduct(resp.data[0]);
			},
			(err) => {
				getProps({ variant: 'danger', message: err });

			},
		);
	}, [id]); // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		fetchProductDetail();
	}, [fetchProductDetail]);

	useEffect(() => {
		let arr: any = [];
		categories.forEach((cat: any) => {
			arr.push({ value: cat.id, text: cat.title });
		});

		setCategoriesList(arr);
	}, [categories]);

	useEffect(() => {
		let formData = FormDataHelper();
		GeneralServices.postRequest(formData, LIST_CATEGORIES).then(
			(resp) => {
				setCategories(resp.data);
			},
			(err) => {
				showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
			},
		);
	}, []);

	function deleteImage(imageId: any) {
		Swal.fire({
			title: 'MedSouk',
			text: 'Are you sure you want to delete the store image?',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.isConfirmed) {
				if (!id) return;
				let helper = FormDataHelper();
				helper.append('product_id', id);
				helper.append('delete_image_type', 'product');
				helper.append('image_id', imageId);

				GeneralServices.postRequest(helper, DELETE_IMAGE).then(
					(resp) => {
						fetchProductDetail();
						getProps({ variant: 'success', message: resp.message });
					},
					(err) => {
						getProps({ variant: 'danger', message: err });
						// showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
					},
				);
			}
		});
	}

	return (
		<PageWrapper>
			<Page>
				<div className='row'>
					<div className='col-lg-12'>
						<Card stretch>
							<CardHeader>
								<CardLabel icon='FormatSize' iconColor='warning'>
									<CardTitle>Products</CardTitle>
									<CardSubTitle>Update Product</CardSubTitle>
								</CardLabel>
							</CardHeader>

							<CardBody>
								{isVisible ? <AlertMessage /> : ''}
								<form className='row g-4' noValidate onSubmit={formik.handleSubmit}>
									<FormGroup className='col-12' id='name' label='Name'>
										<Input
											placeholder='Enter Product Name'
											onChange={formik.handleChange}
											value={formik.values.name}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.name}
											invalidFeedback={formik.errors.name}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup className='col-4' id='product_price' label='Price'>
										<Input
											placeholder='Enter Product Price'
											type='number'
											onChange={formik.handleChange}
											value={formik.values.product_price}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.product_price}
											invalidFeedback={formik.errors.product_price}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup
										id='currency_id'
										label='Please Select Currency'
										className='col-4'>
										<Select
											ariaLabel='currency'
											required
											placeholder='---Select Currency---'
											onChange={formik.handleChange}
											value={formik.values.currency_id}
											list={CURRENCY_OPTIONS}
											isTouched={formik.touched.currency_id}
											invalidFeedback={formik.errors.currency_id}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup
										id='category_id'
										label='Please Select Category'
										className='col-4'>
										<Select
											ariaLabel='category'
											required
											placeholder='---Select Category---'
											onChange={formik.handleChange}
											value={formik.values.category_id}
											list={categoriesList}
											isTouched={formik.touched.category_id}
											invalidFeedback={formik.errors.category_id}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<FormGroup
										className='col-12'
										id='short_description'
										label='Short Description'>
										<Textarea
											placeholder='Enter Short Description'
											onChange={formik.handleChange}
											value={formik.values.short_description}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.short_description}
											invalidFeedback={formik.errors.short_description}
											validFeedback='Looks good!'></Textarea>
									</FormGroup>
									<FormGroup
										className='col-12'
										id='long_description'
										label='Long Description'>
										<Textarea
											placeholder='Enter Long Description'
											onChange={formik.handleChange}
											value={formik.values.long_description}
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.long_description}
											invalidFeedback={formik.errors.long_description}
											validFeedback='Looks good!'></Textarea>
									</FormGroup>
									<FormGroup
										className='col-12'
										id='formFile'
										label='Product picture'>
										<Input
											type='file'
											ref={inputFileRef}
											accept='image/png, image/jpeg'
											onChange={(event: any) =>
												formik.setFieldValue(
													'formFile',
													event.currentTarget.files,
												)
											}
											// value={formik.values.formFile}
											multiple
											onBlur={formik.handleBlur}
											isValid={formik.isValid}
											isTouched={formik.touched.formFile}
											invalidFeedback={formik.errors.formFile}
											validFeedback='Looks good!'
										/>
									</FormGroup>
									<div className='col'>
										<Button
											type='submit'
											color='info'
											icon='Save'
											isDisable={!formik.isValid}>
											Save
										</Button>
									</div>
								</form>
							</CardBody>
						</Card>
					</div>
					{product &&
						product.images_data &&
						product.images_data.length > 0 &&
						product.images_data[0].image_id != '0' && (
							<div className='col-lg-12'>
								{/* <div className='col-4'> */}
								<Card stretch>
									<CardHeader>
										<CardLabel icon='ContentCopy' iconColor='dark'>
											<CardTitle>Product Images</CardTitle>
											<CardSubTitle>{product.title}</CardSubTitle>
										</CardLabel>
									</CardHeader>
									<CardBody>
										<div className='row g-4'>
											{product.images_data.map((item: any, key: any) => (
												<div key={item} className='col-lg-2'>
													<Card shadow={'3d'} className='mb-0'>
														<CardHeader>
															<CardLabel>
																<CardTitle>
																	{item.image_name}
																</CardTitle>
															</CardLabel>
														</CardHeader>
														<CardBody>
															<div className='col-lg-auto'>
																<Avatar
																	// srcSet={USERS.JOHN.srcSet}
																	src={item.image_path}
																	color={'brand'}
																	rounded={3}
																/>
															</div>
														</CardBody>
														<CardFooter>
															<Button
																onClick={() =>
																	deleteImage(item.image_id)
																}
																color='dark'
																isLight
																icon='Delete'>
																Delete Product Image
															</Button>
														</CardFooter>
													</Card>
												</div>
											))}
										</div>
									</CardBody>
								</Card>
								{/* </div> */}
							</div>
						)}
				</div>
			</Page>
		</PageWrapper>
	);
};
export default EditProduct;
