import React, { useContext, useEffect, useState } from 'react';
import { useFormik } from 'formik';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PaginationButtons, {
  dataPagination,
  PER_COUNT,
} from '../../../../components/PaginationButtons';
import useSortableData from '../../../../hooks/useSortableData';
import useSelectTable from '../../../../hooks/useSelectTable';
import Icon from '../../../../components/icon/Icon';
// import Modal, {
// 	ModalBody,
// 	ModalFooter,
// 	ModalHeader,
// 	ModalTitle,
// } from './../../components/bootstrap/Modal';
import Card, {
  CardBody,
  CardHeader,
  CardLabel,
  CardTitle,
} from '../../../../components/bootstrap/Card';
import CategoryTableRow from '../../../_common/CategoryTableRow';
import ProductTableRow from '../../../_common/ProductTableRow';
import SubHeader, { SubHeaderLeft } from '../../../../layout/SubHeader/SubHeader';
import Input from '../../../../components/bootstrap/forms/Input';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { LIST_PRODUCTS } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import { HashLoader } from 'react-spinners';
import DataErrorComponent from '../../../_common/NoDataErrorComponent';
// import './../categories/categories.css'
import Modal,{ ModalBody,
	ModalFooter,
 	ModalHeader,
 	ModalTitle} from '../../../../components/bootstrap/Modal';
import Button from '../../../../components/bootstrap/Button';

const ListLeaveBalance = () => {
  const [products, setProducts] = useState<any[]>([]);
  const [filteredData, setFilteredData] = useState<any[]>([]);
  const { userData } = useContext(AuthContext);

  
  const [centeredStatus, setCenteredStatus] = useState(false);
  const [state, setState] = useState(false);
  const [staticBackdropStatus, setStaticBackdropStatus] = useState(false);
  const [scrollableStatus, setScrollableStatus] = useState(false);

const [sizeStatus, setSizeStatus] = useState(null);
// const [fullScreenStatus, setFullScreenStatus] = useState<TModalFullScreen | undefined>(
// 	undefined,
// );
const [animationStatus, setAnimationStatus] = useState(true);
const [longContentStatus, setLongContentStatus] = useState(false);
const [headerCloseStatus, setHeaderCloseStatus] = useState(true);
const [veiwReason , setViewreason] = useState(false)
  const formik = useFormik({
    initialValues: {
      searchInput: '',
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    onSubmit: (values) => {
      
      // alert(JSON.stringify(values, null, 2));
    },
  });

  const [currentPage, setCurrentPage] = useState<number>(1);
  const [perPage, setPerPage] = useState<number>(PER_COUNT['10']);
	const [loading, setLoading] = useState(true);
  const [error, setError] = useState([]);
  const { items, requestSort, getClassNamesFor } = useSortableData(filteredData);
  const onCurrentPageItems = dataPagination(items, currentPage, perPage);
  const { selectTable, SelectAllCheck } = useSelectTable(onCurrentPageItems);

  const [searchValue, setSearchValue] = useState<string>('');
  const searchOnChange = (e: any) => {
    setSearchValue(e.target.value);

    const searchedData = products.filter((item) => {
      const query = e.target.value.toLowerCase();

      return (
        item.id.toLowerCase().indexOf(query) >= 0 ||
        item.title.toLowerCase().indexOf(query) >= 0 ||
        item.price.toLowerCase().indexOf(query) >= 0
        // item.category_title.toLowerCase().indexOf(query) >= 0
      );
    });

    setFilteredData(searchedData);
  };

  useEffect(() => {
    if (!userData.pharmacy_id) return;

    let helper = FormDataHelper();
    helper.append('pharmacy_id', userData.pharmacy_id);

    GeneralServices.postRequest(helper, LIST_PRODUCTS).then(
      (resp) => {
     
        setProducts(resp.data);
        setLoading(false);
      },
      (err) => {
        setLoading(false);
				setError(err)
      },
    );
  }, [userData.pharmacy_id]);

  useEffect(() => {
    setFilteredData(products);
  }, [products]);
  return (
    <PageWrapper title={'List Products'}>
      <SubHeader>
        <SubHeaderLeft>
          <label
            className='border-0 bg-transparent cursor-pointer me-0'
            htmlFor='searchInput'>
            <Icon icon='Search' size='2x' color='primary' />
          </label>
          <Input
            id='searchInput'
            type='search'
            className='border-0 shadow-none bg-transparent'
            placeholder='Search Employe... (id, title and name)'
            onChange={searchOnChange}
            value={searchValue}
          />
        </SubHeaderLeft>
      </SubHeader>
      <Button
                className='me-4'
                color='primary'
                isLight
                icon='Preview'
                onClick={() => {
                    // initialStatus();
                    setCenteredStatus(true);
                    setState(true);
                }}>
               
            </Button>
      <Page>
        <Card stretch data-tour='list'>
          <CardHeader>
            <CardLabel>
              <CardTitle>
                Leaves Balance
                <small className='ms-2'>
                  Total:
                  {selectTable.values.selectedList.length
                    ? `${selectTable.values.selectedList.length} / `
                    : null}
                  {filteredData.length}
                </small>
              </CardTitle>
            </CardLabel>
          </CardHeader>
          <CardBody className='table-responsive' isScrollable>
          
              <table className='table table-modern table-hover'>
                <thead>
                  <tr>
                    <th
                      scope='col'
                      onClick={() => requestSort('id')}
                      className='cursor-pointer text-decoration-underline'>
                      #
                      <Icon
                        size='lg'
                        className={getClassNamesFor('id')}
                        icon='FilterList'
                      />
                    </th>
                    <th scope='col'>Image</th>
                    <th scope='col'>Name</th>
                    <th scope='col'>Previous Year</th>
                    <th scope='col'>Current Year
</th>
                    <th scope='col'>Total
</th>

                    <th scope='col'>Used</th>
                    <th scope='col'>Accepted
</th>

                    <th scope='col' className='text-center'>
                    Rejected
                    </th>
                    <th scope='col' className='text-center'>
                    Expired

                    </th>
                    <th scope='col' className='text-center'>
                    Carry Over


                    </th>
                  </tr>
                </thead>
                <tbody>
{onCurrentPageItems.length > 0 ? onCurrentPageItems.map((i) => (
                    <ProductTableRow
                      key={i.id}
                      // eslint-disable-next-line react/jsx-props-no-spreading
                      {...i}
                      selectName='selectedList'
                      selectOnChange={selectTable.handleChange}
                      selectChecked={selectTable.values.selectedList.includes(
                        // @ts-ignore
                        i.id.toString(),
                      )}
                    />
                  )):( "  "
                    // <div className='empty-table-message'><DataErrorComponent  loading={loading}
                    // error={error}
                    // length={products.length}/></div>
                    )}
                  {}
                </tbody>
              </table>
            
          </CardBody>
          <PaginationButtons
            data={items}
            label='items'
            setCurrentPage={setCurrentPage}
            currentPage={currentPage}
            perPage={perPage}
            setPerPage={setPerPage}
          />
        </Card>
      </Page>
{/* <ModalCenter Open={veiwReason} name={ `Aswad`} reason={"Something"}/> */}
<Modal
					isOpen={state}
					setIsOpen={setState}
					titleId='exampleModalLabel'
					isStaticBackdrop={staticBackdropStatus}
					isScrollable={scrollableStatus}
					isCentered={centeredStatus}
					size={sizeStatus}
					// fullScreen={fullScreenStatus}
					isAnimation={animationStatus}>
					<ModalHeader setIsOpen={headerCloseStatus ? setState : undefined}>
						<ModalTitle id='exampleModalLabel'>Aswad</ModalTitle>
					</ModalHeader>
					<ModalBody>Aswad</ModalBody>
					<ModalFooter>
						<Button
							color='info'
							isOutline
							className='border-0'
							onClick={() => setState(false)}>
							Close
						</Button>
						
					</ModalFooter>
				</Modal>
    </PageWrapper>
  );
};

export default ListLeaveBalance;
