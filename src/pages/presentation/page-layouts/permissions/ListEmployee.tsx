import React, { useState, useEffect } from 'react';

import { useFormik } from 'formik';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PaginationButtons, {
	dataPagination,
	PER_COUNT,
} from '../../../../components/PaginationButtons';
import useDarkMode from '../../../../hooks/useDarkMode';
import useSortableData from '../../../../hooks/useSortableData';
import useSelectTable from '../../../../hooks/useSelectTable';
import Icon from '../../../../components/icon/Icon';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,  

} from '../../../../components/bootstrap/Card';
import OrderTableRow from '../../../_common/OrderTableRow';
import ListCustomersTableRow from '../../../_common/ListCustomersTableRow';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { LIST_CUSTOMERS } from '../../../../services/axiousURL';
import DataErrorComponent from '../../../_common/NoDataErrorComponent';


const ListCustomers = () => {
	const data: {
		id: number;
		customer_name: string;
		phone_number: string;
		address: string;
		total_orders: string;
		join_date: string;
	}[] = [
		{
			id: 1,
			customer_name: 'Jon Doe',
			phone_number: '+1-333-41233',
			address: 'lorem lipsum',
			join_date: '2023/04/10',
			total_orders: '300',
		},
	];

	const [customers, setCustomers] = useState<any[]>([]);
	const [filteredData, setFilteredData] = useState<any[]>([]);

	const [currentPage, setCurrentPage] = useState<number>(1);
	const [perPage, setPerPage] = useState<number>(PER_COUNT['10']);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState([]);


	const { items, requestSort, getClassNamesFor } = useSortableData(filteredData);
	const onCurrentPageItems = dataPagination(items, currentPage, perPage);
	const { selectTable, SelectAllCheck } = useSelectTable(onCurrentPageItems);


	
	useEffect(() => {
		setFilteredData(customers);
	}, [customers]);

	useEffect(() => {
		let helper = FormDataHelper();

		GeneralServices.postRequest(helper, LIST_CUSTOMERS).then(
			(resp) => {
				setCustomers(resp.data);
				setLoading(false);

			},
			(err) => {
				setLoading(false);
				setError(err)

			},
		);
	}, []);

	return (
		<PageWrapper title={'Customers'}>
			<Page>
				<Card stretch data-tour='list'>
					<CardHeader>
						<CardLabel>
							<CardTitle>
							Employee Access Control 
								{/* <small className='ms-2'>
									Item:{' '}
									{selectTable.values.selectedList.length
										? `${selectTable.values.selectedList.length} / `
										: null}
									{filteredData.length}
								</small> */}
							</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody className='table-responsive' isScrollable>
						<table className='table table-modern table-hover'>
							<thead>
								<tr>
									{/* <th
										scope='col'
										onClick={() => requestSort('order_id')}
										className='cursor-pointer text-decoration-underline'>
										#
										<Icon
											size='lg'
											className={getClassNamesFor('order_id')}
											icon='FilterList'
										/>
									</th> */}
									 <th scope="col">Employee Name</th>
      <th scope="col">Employee ID</th>
      <th scope="col">Department</th>
      <th scope="col">Position</th>
      <th scope="col">Permissions</th>
      <th scope="col">Actions</th>
								</tr>
							</thead>
							<tbody>
								{ onCurrentPageItems.length > 0 ? onCurrentPageItems.map((i) => (
									<ListCustomersTableRow
										key={i.id}
										// eslint-disable-next-line react/jsx-props-no-spreading
										{...i}
										selectName='selectedList'
										selectOnChange={selectTable.handleChange}
										selectChecked={selectTable.values.selectedList.includes(
											// @ts-ignore
											i.id.toString(),
										)}
									/>
								)):                    (<div className='empty-table-message'><DataErrorComponent  loading={loading}
								error={error}
								length={customers.length}/></div>)}
							</tbody>
						</table>
					</CardBody>
					<PaginationButtons
						data={items}
						label='items'
						setCurrentPage={setCurrentPage}
						currentPage={currentPage}
						perPage={perPage}
						setPerPage={setPerPage}
					/>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default ListCustomers;
