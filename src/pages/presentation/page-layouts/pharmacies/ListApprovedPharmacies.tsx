import React, { useState, useEffect } from 'react';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import useSortableData from '../../../../hooks/useSortableData';
import PaginationButtons, {
	PER_COUNT,
	dataPagination,
} from '../../../../components/PaginationButtons';
import useSelectTable from '../../../../hooks/useSelectTable';
import { GeneralServices } from '../../../../services/General.Services';
import { LIST_PHARMACIES } from '../../../../services/axiousURL';
import Icon from '../../../../components/icon/Icon';
import Card, {
	CardHeader,
	CardLabel,
	CardTitle,
	CardBody,
} from '../../../../components/bootstrap/Card';
import Page from '../../../../layout/Page/Page';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import PendingPharmacyTableRow from '../../../_common/PendingPharmacyTableRow';
import DataErrorComponent from '../../../_common/NoDataErrorComponent';

const ListApprovedPharmacies = () => {
	const [pharmacies, setPharmacies] = useState<any[]>([]);
	const [filteredData, setFilteredData] = useState<any[]>([]);

	const [loading, setLoading] = useState(true);
	const [error, setError] = useState([]);

	const [currentPage, setCurrentPage] = useState<number>(1);
	const [perPage, setPerPage] = useState<number>(PER_COUNT['10']);

	const { items, requestSort, getClassNamesFor } = useSortableData(filteredData);
	const onCurrentPageItems = dataPagination(items, currentPage, perPage);
	const { selectTable, SelectAllCheck } = useSelectTable(onCurrentPageItems);

	const removePharmacy = (pharmacyId: string) => {
		let pharms = filteredData;
		pharms = pharms.filter((single: any) => single.id != pharmacyId);
		setFilteredData(pharms);
	};

	useEffect(() => {
		setFilteredData(pharmacies);
	}, [pharmacies]);

	useEffect(() => {
		let helper = FormDataHelper();
		GeneralServices.postRequest(helper, LIST_PHARMACIES).then(
			(resp) => {
				let pharms: any[] = resp.data;
				pharms = pharms.filter((single: any) => single.status == 'Y');
				setPharmacies(pharms);
				setLoading(false);
			},
			(err) => {
				setLoading(false);
				setError(err)
			},
		);
	}, []);

	return (
		<PageWrapper title={'List Pharmacies'}>
			<Page>
				<Card stretch data-tour='list'>
					<CardHeader>
						<CardLabel>
							<CardTitle>
								List Pending Pharmacies
								<small className='ms-2'>
									Item:
									{selectTable.values.selectedList.length
										? `${selectTable.values.selectedList.length} / `
										: null}
									{filteredData.length}
								</small>
							</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody className='table-responsive' isScrollable>
						<table className='table table-modern table-hover'>
							<thead>
								<tr>
									<th
										scope='col'
										onClick={() => requestSort('id')}
										className='cursor-pointer text-decoration-underline'>
										#
										<Icon
											size='lg'
											className={getClassNamesFor('id')}
											icon='FilterList'
										/>
									</th>
									<th scope='col'>Pharmacy Name</th>
									<th scope='col'>Pharmacy Type</th>
									<th scope='col'>Admin Username</th>
									<th scope='col'>Admin Phone Number</th>
									<th scope='col'>Admin Email Address</th>
									<th scope='col'>Status</th>
								</tr>
							</thead>
							<tbody>
								{ onCurrentPageItems.length > 0 ? onCurrentPageItems.map((i) => (
									<PendingPharmacyTableRow
										key={i.id}
										// eslint-disable-next-line react/jsx-props-no-spreading
										{...i}
										removePharmacy={removePharmacy}
										selectName='selectedList'
										selectOnChange={selectTable.handleChange}
										selectChecked={selectTable.values.selectedList.includes(
											// @ts-ignore
											i.id.toString(),
										)}
									/>
								)):(
									<div className='empty-table-message'><DataErrorComponent  loading={loading}
									error={error}
									length={pharmacies.length}/></div>)}
							</tbody>
						</table>
					</CardBody>
					<PaginationButtons
						data={items}
						label='items'
						setCurrentPage={setCurrentPage}
						currentPage={currentPage}
						perPage={perPage}
						setPerPage={setPerPage}
					/>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default ListApprovedPharmacies;
