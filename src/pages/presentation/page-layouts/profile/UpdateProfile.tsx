import React, { useContext, useEffect, useState } from 'react';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../components/bootstrap/forms/Input';
import Button from '../../../../components/bootstrap/Button';
import { useFormik } from 'formik';
import showNotification from '../../../../components/extras/showNotification';
import Select from '../../../../components/bootstrap/forms/Select';
import AuthContext from '../../../../contexts/authContext';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { LIST_PHARMACIES, UPDATE_PHARMACY_PROFILE } from '../../../../services/axiousURL';
import Popovers from '../../../../components/bootstrap/Popovers';
import { useAlert } from '../../../../hooks/useAlert';
import regexs from '../../../../constant/Regexs';
import Toasts from '../../../../components/bootstrap/Toasts';


const UpdateProfile = () => {
	const { userData, setUser } = useContext(AuthContext);
	const [AlertMessage, isVisible, getProps] = useAlert();
	const [pharmacy, setPharmacy] = useState<any>({
		title: '',
		registration_number: '',
		admin_phone_number: '',
		type: 'Pharmacy',
		admin_email_address: '',
		admin_username: '',
	});
	const  [loader, setLoader] = useState(false)

	const TYPE_OPTIONS = [
		{ value: 'Pharmacy', text: 'Pharmacy' },
		{ value: 'Labs', text: 'Labs' },
		{ value: 'Hospitals', text: 'Hospitals' },
	];

	const validate = (values: {
		title: string;
		registration_number: string;
		admin_phone_number: string;
		type: string;
		admin_email_address: string;
		admin_username: string;
		admin_password: string;
		admin_confirm_password: string;
	}) => {
		const errors: {
			title?: string;
			registration_number?: string;
			admin_phone_number?: string;
			type?: string;
			admin_email_address?: string;
			admin_username?: string;
			admin_password?: string;
			admin_confirm_password?: string;
		} = {};
		if (!values.admin_email_address) {
			errors.admin_email_address = 'Required';
		} else if (!regexs.emailRegex.regex.test(values.admin_email_address)) {
			errors.admin_email_address = regexs.emailRegex.errMessage;
		}

		if (!values.admin_username) {
			errors.admin_username = 'Required';
		} else if (!regexs.usernameRegex.regex.test(values.admin_username)) {
			errors.admin_username = regexs.usernameRegex.errMessage;
		}

		if (!values.title) {
			errors.title = 'Required';
		} else if (values.title.length < 5) {
			errors.title = 'Name must be 5 characters or more';
		}

		if (!values.admin_phone_number) {
			errors.admin_phone_number = 'Required';
		} else if (!regexs.phoneRegex.regex.test(values.admin_phone_number)) {
			errors.admin_phone_number = regexs.phoneRegex.errMessage;
		}

		if (!values.type) {
			errors.type = 'Required';
		}

		if (values.admin_password) {
			if (!values.admin_confirm_password) {
				errors.admin_confirm_password = 'Required';
			} else if (!regexs.passwordRegex.regex.test(values.admin_password)) {
				errors.admin_password = regexs.passwordRegex.errMessage;
			} else if (values.admin_password != values.admin_confirm_password) {
				errors.admin_password = 'Password and Confirm password does not match';
			}
		}
		return errors;
	};

	const formik = useFormik({
		initialValues: {
			title: pharmacy.title,
			registration_number: pharmacy.registration_number,
			admin_phone_number: pharmacy.admin_phone_number,
			type: pharmacy.type,
			admin_email_address: pharmacy.admin_email_address,
			admin_username: pharmacy.admin_username,
			admin_password: '',
			admin_confirm_password: '',
		},
		enableReinitialize: true,
		validate,
		onSubmit: (values) => {
			let formData = FormDataHelper();
			formData.append('id', userData.pharmacy_id!);
			formData.append('admin_id', userData.admin_id!);
			formData.append('type', values.type);
			formData.append('admin_phone_number', values.admin_phone_number);
			formData.append('admin_username', values.admin_username);
			formData.append('admin_email_address', values.admin_email_address);
			if (values.admin_password) formData.append('admin_password', values.admin_password);

			GeneralServices.postRequest(formData, UPDATE_PHARMACY_PROFILE).then(
				(resp) => {
					getProps({ variant: 'success', message: resp.message });
					if (setUser) setUser(values.admin_username);
				getProps({ variant: 'success', message: resp.message });

				},
				(err) => {
					getProps({ variant: 'danger', message: err });
				},
			);
		},
	});

	useEffect(() => {
		if (!userData.pharmacy_id) return;
		let helper = FormDataHelper();
		helper.append('id', userData.pharmacy_id);
  setLoader(true)
		GeneralServices.postRequest(helper, LIST_PHARMACIES).then(
			(resp) => {
				setPharmacy(resp.data[0]);
				setLoader(false)
			},
			(err) => {
				getProps({ variant: 'danger', message: `${err} Please Try Again Later!` });
				setLoader(false)

					
			},
		);
	}, [userData.pharmacy_id]);// eslint-disable-line react-hooks/exhaustive-deps

	return (
		<PageWrapper>
			<Page>
				{/* label */}
				<div className='col-lg-12 h-100'>
					<Card>
						<CardHeader>
							<CardLabel icon='FormatSize' iconColor='warning'>
								<CardTitle>Human Resource</CardTitle>
								<CardSubTitle>Update Profile Information</CardSubTitle>
							</CardLabel>
						</CardHeader>

						<CardBody>
							{isVisible ? <AlertMessage /> : ''}
							<form className='row g-4' noValidate onSubmit={formik.handleSubmit}>
								<FormGroup className='col-4' id='name' label='Name'>
									<Input
										placeholder='Name'
										onChange={formik.handleChange}
										value={formik.values.title}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.title}
										invalidFeedback={formik.errors.title}
										validFeedback='Looks good!'
										readOnly
										disabled
									/>
								</FormGroup>

								{/* <FormGroup id='type' label='Please Select Type' className='col-4'>
									<Select
										ariaLabel='type'
										required
										placeholder='---Select Type---'
										onChange={formik.handleChange}
										value={formik.values.type}
										list={TYPE_OPTIONS}
										isTouched={formik.touched.type}
										invalidFeedback={formik.errors.type}
										validFeedback='Looks good!'
									/>
								</FormGroup> */}

								<FormGroup
									className='col-4'
									id='admin_phone_number'
									label='Phone Number'>
									<Input
										type='text'
										placeholder='Enter Phone Number'
										required
										onChange={formik.handleChange}
										value={formik.values.admin_phone_number}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.admin_phone_number}
										invalidFeedback={formik.errors.admin_phone_number}
										validFeedback='Looks good!'
									/>
								</FormGroup>

								<FormGroup
									className='col-4'
									id='admin_username'
									label='Username'>
									<Input
										type='text'
										placeholder='Enter Phone Number'
										required
										onChange={formik.handleChange}
										value={formik.values.admin_username}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.admin_username}
										invalidFeedback={formik.errors.admin_username}
										validFeedback='Looks good!'
									/>
								</FormGroup>

								<FormGroup
									className='col-4'
									id='admin_email_address'
									label='Email address'>
									<Input
										type='text'
										placeholder='Enter admin email'
										required
										onChange={formik.handleChange}
										value={formik.values.admin_email_address}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.admin_email_address}
										invalidFeedback={formik.errors.admin_email_address}
										validFeedback='Looks good!'
										readOnly
										disabled
									/>
								</FormGroup>

								<FormGroup
									className='col-4'
									id='admin_password'
									label='Password'>
									<Popovers
										desc="Leave the password fields blank, if you don't want to change"
										placement='auto'
										title='Medsouk'
										trigger='hover'>
										{`  ?`}
									</Popovers>
									<Input
										type='password'
										name='admin_password'
										placeholder='Enter password'
										onChange={formik.handleChange}
										value={formik.values.admin_password}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.admin_password}
										invalidFeedback={formik.errors.admin_password}
										validFeedback='Looks good!'
										
									/>
								</FormGroup>

								<FormGroup
									className='col-4'
									id='admin_confirm_password'
									label='Confirm Password'>
									<Input
										type='password'
										placeholder='Enter password'
										onChange={formik.handleChange}
										value={formik.values.admin_confirm_password}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.admin_confirm_password}
										invalidFeedback={formik.errors.admin_confirm_password}
										validFeedback='Looks good!'
									/>
								</FormGroup>

								<div className='col-12'>
									<Button
										type='submit'
										color='info'
										icon='Save'
										isDisable={!formik.isValid}>
										Save
									</Button>
								</div>
							</form>
						</CardBody>
					</Card>
				</div>
			</Page>
		</PageWrapper>
	);
};

export default UpdateProfile;
