import React, { useContext, useEffect, useState } from 'react';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../components/bootstrap/forms/Input';
import Button from '../../../../components/bootstrap/Button';
import { useFormik } from 'formik';
import AuthContext from '../../../../contexts/authContext';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import {
	LIST_PHARMACIES,
	UPDATE_PHARMACY_PROFILE,
	UPDATE_SUPER_ADMIN_PROFILE,
} from '../../../../services/axiousURL';
import Popovers from '../../../../components/bootstrap/Popovers';
import { useAlert } from '../../../../hooks/useAlert';
import regexs from '../../../../constant/Regexs';

const UpdateSuperAdminProfile = () => {
	const { userData, setUser } = useContext(AuthContext);
	const [AlertMessage, isVisible, getProps] = useAlert();
	const [pharmacy, setPharmacy] = useState<any>({
		phone_number: '',
		email_address: '',
		username: '',
	});

	const validate = (values: {
		phone_number: string;
		email_address: string;
		username: string;
		password: string;
		confirm_password: string;
	}) => {
		const errors: {
			phone_number?: string;
			email_address?: string;
			username?: string;
			password?: string;
			confirm_password?: string;
		} = {};
		if (!values.username) {
			errors.username = 'Required';
		} else if (!regexs.nameRegex.regex.test(values.username)) {
			errors.username = regexs.nameRegex.errMessage;
		}
		if (!values.phone_number) {
			errors.phone_number = 'Required';
		} else if (!regexs.phoneRegex.regex.test(values.phone_number)) {
			errors.phone_number = regexs.phoneRegex.errMessage;
		}

		if (values.password) {
			if (!values.confirm_password) {
				errors.confirm_password = 'Required';
			} else if (values.password != values.confirm_password) {
				errors.password = 'Password and Confirm password does not match';
			}
		}
		return errors;
	};

	const formik = useFormik({
		initialValues: {
			phone_number: pharmacy.admin_phone_number,
			email_address: pharmacy.admin_email_address,
			username: pharmacy.admin_username,
			password: '',
			confirm_password: '',
		},
		enableReinitialize: true,
		validate,
		onSubmit: (values) => {
			let formData = FormDataHelper();
			formData.append('super_admin_id', userData.admin_id!);
			formData.append('phone_number', values.phone_number);
			formData.append('name', values.username);
			if (values.password) formData.append('admin_password', values.password);

			GeneralServices.postRequest(formData, UPDATE_SUPER_ADMIN_PROFILE).then(
				(resp) => {
					getProps({ variant: 'success', message: resp.message });
					if (setUser) setUser(values.username);
				getProps({ variant: 'success', message: resp.message });

				},
				(err) => {
					getProps({ variant: 'danger', message: err });
				},
			);
		},
	});

	useEffect(() => {
		if (!userData.pharmacy_id) return;
		let helper = FormDataHelper();
		helper.append('id', userData.pharmacy_id);

		GeneralServices.postRequest(helper, LIST_PHARMACIES).then(
			(resp) => {
				setPharmacy(resp.data[0]);
			},
			(err) => {
				getProps({ variant: 'danger', message: `${err} Please Try Again Later!` });
				

			},
		);
	}, [userData.pharmacy_id]);// eslint-disable-line react-hooks/exhaustive-deps

	return (
		<PageWrapper>
			<Page>
				{/* label */}
				<div className='col-lg-12 h-100'>
					<Card>
						<CardHeader>
							<CardLabel icon='FormatSize' iconColor='warning'>
								<CardTitle>Profile</CardTitle>
								<CardSubTitle>Update Super Admin Profile</CardSubTitle>
							</CardLabel>
						</CardHeader>

						<CardBody>
							{isVisible ? <AlertMessage /> : ''}
							<form className='row g-4' noValidate onSubmit={formik.handleSubmit}>
								<FormGroup className='col-12' id='username' label='Name'>
									<Input
										placeholder='UserName'
										onChange={formik.handleChange}
										value={formik.values.username}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.username}
										invalidFeedback={formik.errors.username}
										validFeedback='Looks good!'
									/>
								</FormGroup>

								<FormGroup className='col-4' id='phone_number' label='Phone Number'>
									<Input
										type='text'
										placeholder='Enter Phone Number'
										required
										onChange={formik.handleChange}
										value={formik.values.phone_number}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.phone_number}
										invalidFeedback={formik.errors.phone_number}
										validFeedback='Looks good!'
									/>
								</FormGroup>

								<FormGroup
									className='col-4'
									id='admin_email_address'
									label='Email address'>
									<Input
										type='text'
										placeholder='Enter email address'
										required
										onChange={formik.handleChange}
										value={formik.values.email_address}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.email_address}
										invalidFeedback={formik.errors.email_address}
										validFeedback='Looks good!'
										readOnly
										disabled
									/>
								</FormGroup>

								<FormGroup className='col-4' id='password' label='Password'>
									<Popovers
										desc="Leave the password fields blank, if you don't want to change"
										placement='auto'
										title='Medsouk'
										trigger='hover'>
										{`  ?`}
									</Popovers>
									<Input
										type='password'
										name='password'
										placeholder='Enter password'
										onChange={formik.handleChange}
										value={formik.values.password}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.password}
										invalidFeedback={formik.errors.password}
										validFeedback='Looks good!'
									/>
								</FormGroup>

								<FormGroup
									className='col-4'
									id='confirm_password'
									label='Confirm Password'>
									<Input
										type='password'
										placeholder='Enter confirm password'
										onChange={formik.handleChange}
										value={formik.values.confirm_password}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.confirm_password}
										invalidFeedback={formik.errors.confirm_password}
										validFeedback='Looks good!'
									/>
								</FormGroup>

								<div className='col-12'>
									<Button
										type='submit'
										color='info'
										icon='Save'
										isDisable={!formik.isValid}>
										Save
									</Button>
								</div>
							</form>
						</CardBody>
					</Card>
				</div>
			</Page>
		</PageWrapper>
	);
};
export default UpdateSuperAdminProfile;
