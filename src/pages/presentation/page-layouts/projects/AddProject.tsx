import React, { useContext } from 'react';
import { useFormik } from 'formik';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../components/bootstrap/forms/Input';
import Page from '../../../../layout/Page/Page';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import showNotification from '../../../../components/extras/showNotification';
import Button from '../../../../components/bootstrap/Button';
import Textarea from '../../../../components/bootstrap/forms/Textarea';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { ADD_CATEGORY } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import { useAlert } from '../../../../hooks/useAlert';
import Select from '../../../../components/bootstrap/forms/Select';
import { DEPARTMENT_OPTIONS, PROJECT_PRIORITY } from '../../../../constant/options';
import MultipleSelectCheckmarks from '../../../../components/bootstrap/forms/CheckMarks';

const AddCategory = () => {
	const { userData } = useContext(AuthContext);
	const [AlertMessage, isVisible, getProps] = useAlert();
	const validate = (values: { title: string; Department:string; description?: string; from:string; to:string; team:any }) => {
		const errors: {
			title?: string;
			description?: string;
			Department?:string
			from?:string;
			 to?:string
			 team?:any
		} = {};
		if (!values.title) {
			errors.title = 'Required';
		} else if (values.title.length < 3) {
			errors.title = 'Title should be equal to or greater than 3 characters*';
		}
		return errors;
	};

	const formik = useFormik({
		initialValues: {
			title: '',
			Department:'',
			description: '',
			from:'',
			to:'',
			team: [],
		},
		validate,
		onSubmit: (values) => {
			let formData = FormDataHelper();
			formData.append('title', values.title);
			formData.append('description', values.description);
			formData.append('start_from', values.from);

			formData.append('to_end', values.to);
			formData.append('team', JSON.stringify(values.team));


			formData.append('admin_id', userData.admin_id!);

			GeneralServices.postRequest(formData, ADD_CATEGORY).then(
				(resp) => {
					// showNotification(`${process.env.REACT_APP_SITE_NAME}`, resp.message, 'success');
					getProps({ variant: 'success', message: resp.message });
					formik.resetForm();
				},
				(err) => {
					getProps({ variant: 'danger', message: err });
					// showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
				},
			);
		},
	});

	return (
		<PageWrapper title={'Add Category'}>
			<Page>
				<div className='col-lg-12 h-100'>
					<Card stretch>
						<CardHeader>
							<CardLabel icon='FormatSize' iconColor='warning'>
								<CardTitle>Categories</CardTitle>
								<CardSubTitle>Request Add Category</CardSubTitle>
							</CardLabel>
						</CardHeader>

						<CardBody>
							{isVisible ? <AlertMessage /> : ''}
							<form className='row g-4' noValidate onSubmit={formik.handleSubmit}>
								<FormGroup className='col-6' id='title' label='Project Title'>
									<Input
										placeholder='Enter Project Title'
										onChange={formik.handleChange}
										value={formik.values.title}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.title}
										invalidFeedback={formik.errors.title}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup
									id='Department'
									label='Please Select Department'
									className='col-6'>
									<Select
										ariaLabel='Time'
										placeholder='---Select Department---'
										onChange={formik.handleChange}
										value={formik.values.Department}
										list={DEPARTMENT_OPTIONS}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.Department}
										invalidFeedback={formik.errors.Department}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup
									id='priority'
									label='Please Select Project priority'
									className='col-4'>
									<Select
										ariaLabel='Time'
										placeholder='---Select priority---'
										onChange={formik.handleChange}
										value={formik.values.Department}
										list={PROJECT_PRIORITY}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.Department}
										invalidFeedback={formik.errors.Department}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup className='col-4' id='from' label='From Date'>
									<Input
										type='date'
										placeholder='started From'
										onChange={formik.handleChange}
										value={formik.values.from}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.from}
										invalidFeedback={formik.errors.from}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup className='col-4' id='to' label='To Date'>
									<Input
										type='date'
										placeholder='Enter Leave Name'
										onChange={formik.handleChange}
										value={formik.values.to}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.to}
										invalidFeedback={formik.errors.to}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup
									id='team'
									label='Please Select Project priority'
									className='col-4'>
									<Select
										ariaLabel='Team'
										placeholder='---Select Team---'
										onChange={formik.handleChange}
										value={formik.values.Department}
										list={PROJECT_PRIORITY}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.Department}
										invalidFeedback={formik.errors.Department}
										validFeedback='Looks good!'
									
									/>
								</FormGroup>

								{/* <FormGroup className='col-12' id='description' label='Description'> */}
								<MultipleSelectCheckmarks/>
								{/* </FormGroup> */}
								
								<div className='col'>
									<Button
										type='submit'
										color='info'
										icon='Save'
										isDisable={!formik.isValid}>
										Save
									</Button>
								</div>
							</form>
						</CardBody>
					</Card>
				</div>
			</Page>
		</PageWrapper>
	);
};

export default AddCategory;
