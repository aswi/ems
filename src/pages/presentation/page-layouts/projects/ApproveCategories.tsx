import React, { useEffect, useState } from 'react';

import { useFormik } from 'formik';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PaginationButtons, {
	dataPagination,
	PER_COUNT,
} from '../../../../components/PaginationButtons';
import useSortableData from '../../../../hooks/useSortableData';
import useSelectTable from '../../../../hooks/useSelectTable';
import Icon from '../../../../components/icon/Icon';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import ApproveCategoryTableRow from '../../../_common/ApproveCategoryTableRow';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { LIST_CATEGORIES } from '../../../../services/axiousURL';
import DataErrorComponent from '../../../_common/NoDataErrorComponent';

const ApproveCategories = () => {
	const [categories, setCategories] = useState<any[]>([]);
	const [filteredData, setFilteredData] = useState<any[]>([]);

	const [loading, setLoading] = useState(true);
	const [error, setError] = useState([]);

	const [currentPage, setCurrentPage] = useState<number>(1);
	const [perPage, setPerPage] = useState<number>(PER_COUNT['10']);

	const { items, requestSort, getClassNamesFor } = useSortableData(filteredData);
	const onCurrentPageItems = dataPagination(items, currentPage, perPage);
	const { selectTable, SelectAllCheck } = useSelectTable(onCurrentPageItems);

	const removeCategory = (categoryId: string) => {
		let categores = filteredData;
		categores = categores.filter((single: any) => single.id != categoryId);
		setFilteredData(categores);
	};

	useEffect(() => {
		setFilteredData(categories);
	}, [categories]);

	useEffect(() => {
		let helper = FormDataHelper();
		GeneralServices.postRequest(helper, LIST_CATEGORIES).then(
			(resp) => {
				let cats: any[] = resp.data;
				cats = cats.filter((single: any) => single.status == 'P');
				setCategories(cats);
				setLoading(false);

			},
			(err) => { 
				setLoading(false);
				setError(err)
			},
		);
	}, []);

	return (
		<PageWrapper title={'List Categories'}>
			<Page>
				<Card stretch data-tour='list'>
					<CardHeader>
						<CardLabel>
							<CardTitle>
								Categories List
								<small className='ms-2'>
									Item:
									{selectTable.values.selectedList.length
										? `${selectTable.values.selectedList.length} / `
										: null}
									{filteredData.length}
								</small>
							</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody className='table-responsive' isScrollable>
						<table className='table table-modern table-hover'>
							<thead>
								<tr>
									<th
										scope='col'
										onClick={() => requestSort('id')}
										className='cursor-pointer text-decoration-underline'>
										#
										<Icon
											size='lg'
											className={getClassNamesFor('id')}
											icon='FilterList'
										/>
									</th>
									<th scope='col'>Title</th>
									<th scope='col'>Description</th>
									<th scope='col'>Status</th>
									<th scope='col' className='text-center'>
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								{ onCurrentPageItems.length > 0 ? onCurrentPageItems.map((i) => (
									<ApproveCategoryTableRow
										key={i.id}
										// eslint-disable-next-line react/jsx-props-no-spreading
										{...i}
										removeCategory={removeCategory}
										selectName='selectedList'
										selectOnChange={selectTable.handleChange}
										selectChecked={selectTable.values.selectedList.includes(
											// @ts-ignore
											i.id.toString(),
										)}
									/>
								)):(
									<div className='empty-table-message'><DataErrorComponent  loading={loading}
									error={error}
									length={categories.length}/></div>)}
							</tbody>
						</table>
					</CardBody>
					<PaginationButtons
						data={items}
						label='items'
						setCurrentPage={setCurrentPage}
						currentPage={currentPage}
						perPage={perPage}
						setPerPage={setPerPage}
					/>
				</Card>
			</Page>
		</PageWrapper>
	);
};
export default ApproveCategories;
