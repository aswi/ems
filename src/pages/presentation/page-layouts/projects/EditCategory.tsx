import React, { useContext, useEffect, useState } from 'react';
import { useFormik } from 'formik';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardSubTitle,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Input from '../../../../components/bootstrap/forms/Input';
import Page from '../../../../layout/Page/Page';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import showNotification from '../../../../components/extras/showNotification';
import Button from '../../../../components/bootstrap/Button';
import Textarea from '../../../../components/bootstrap/forms/Textarea';
import { useParams } from 'react-router-dom';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { EDIT_CATEGORY, LIST_CATEGORIES } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import { useAlert } from '../../../../hooks/useAlert';

const EditCategory = () => {
	let { id } = useParams();
	const { userData } = useContext(AuthContext);
	const [AlertMessage, isVisible, getProps] = useAlert();
	const [category, setCategory] = useState<any>({ title: '', description: '' });
	const validate = (values: { title: string; description: string }) => {
		const errors: {
			title?: string;
			description?: string;
		} = {};
		if (!values.title) {
			errors.title = 'Required';
		}

		if (!values.description) {
			errors.description = 'Required';
		}

		return errors;
	};

	const formik = useFormik({
		enableReinitialize: true,
		initialValues: {
			title: category.title,
			description: category.description,
		},
		validate,
		onSubmit: (values) => {
			let formData = FormDataHelper();
			formData.append('admin_id', userData.admin_id!);
			formData.append('category_id', id!);
			formData.append('title', values.title);
			formData.append('description', values.description);

			GeneralServices.postRequest(formData, EDIT_CATEGORY).then(
				(resp) => {
					// showNotification(`${process.env.REACT_APP_SITE_NAME}`, resp.message, 'success');
					getProps({ variant: 'success', message: resp.message });
				},
				(err) => {
					getProps({ variant: 'danger', message: err });
					// showNotification(`${process.env.REACT_APP_SITE_NAME}`, err, 'danger');
				},
			);
		},
	});

	useEffect(() => {
		if (id) {
			let formData = FormDataHelper();
			formData.append('category_id', id);
			GeneralServices.postRequest(formData, LIST_CATEGORIES).then(
				(resp) => {
					let categoryData = resp.data[0];
					setCategory(categoryData);
				},
				(err) => {
					getProps({ variant: 'danger', message: err });
				},
			);
		}
	}, [id]); // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<PageWrapper>
			<Page>
				<div className='col-lg-12 h-100'>
					<Card stretch>
						<CardHeader>
							<CardLabel icon='FormatSize' iconColor='warning'>
								<CardTitle>Categories</CardTitle>
								<CardSubTitle>Edit Requested Category</CardSubTitle>
							</CardLabel>
						</CardHeader>

						<CardBody>
							{isVisible ? <AlertMessage /> : ''}
							<form className='row g-4' noValidate onSubmit={formik.handleSubmit}>
								<FormGroup className='col-12' id='title' label='Category Title'>
									<Input
										placeholder='Enter Category Title'
										onChange={formik.handleChange}
										value={formik.values.title}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.title}
										invalidFeedback={formik.errors.title}
										validFeedback='Looks good!'
									/>
								</FormGroup>
								<FormGroup className='col-12' id='description' label='Description'>
									<Textarea
										placeholder='Enter Description'
										onChange={formik.handleChange}
										value={formik.values.description}
										onBlur={formik.handleBlur}
										isValid={formik.isValid}
										isTouched={formik.touched.description}
										invalidFeedback={formik.errors.description}
										validFeedback='Looks good!'></Textarea>
								</FormGroup>
								<div className='col'>
									<Button
										type='submit'
										color='info'
										icon='Save'
										isDisable={!formik.isValid}>
										Save
									</Button>
								</div>
							</form>
						</CardBody>
					</Card>
				</div>
			</Page>
		</PageWrapper>
	);
};

export default EditCategory;
