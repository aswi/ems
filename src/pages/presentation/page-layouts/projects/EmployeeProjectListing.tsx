import React, { useContext, useEffect, useState } from 'react';

import { useFormik } from 'formik';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PaginationButtons, {
	dataPagination,
	PER_COUNT,
} from '../../../../components/PaginationButtons';
import useSortableData from '../../../../hooks/useSortableData';
import useSelectTable from '../../../../hooks/useSelectTable';
import Icon from '../../../../components/icon/Icon';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import './categories.css';
import CategoryTableRow from '../../../_common/CategoryTableRow';
import { FormDataHelper } from '../../../../services/FormDataHelper';
import { GeneralServices } from '../../../../services/General.Services';
import { LIST_CATEGORIES } from '../../../../services/axiousURL';
import AuthContext from '../../../../contexts/authContext';
import ClipLoader from 'react-spinners/ClipLoader';
import { HashLoader } from 'react-spinners';
import DataErrorComponent from '../../../_common/NoDataErrorComponent';
import Button, { ButtonGroup } from '../../../../components/bootstrap/Button';

const ListEmployeeProjects = () => {
	const { userData } = useContext(AuthContext);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState([]);
	const [categories, setCategories] = useState<any>([]);
	const [filteredData, setFilteredData] = useState<any[]>([]);
	const [showAllCategories, setShowAllCategories] = useState(true);
	const [showApprovedCategories, setShowApprovedCategories] = useState(false);
	const [showPendingCategories, setShowPendingCategories] = useState(false);
	// const filteredData = data;

	const [currentPage, setCurrentPage] = useState<number>(1);
	const [perPage, setPerPage] = useState<number>(PER_COUNT['10']);

	const { items, requestSort, getClassNamesFor } = useSortableData(filteredData);
	const onCurrentPageItems = dataPagination(items, currentPage, perPage);
	const { selectTable, SelectAllCheck } = useSelectTable(onCurrentPageItems);

	useEffect(() => {
		if (categories) setFilteredData(categories);
	}, [categories]);

	useEffect(() => {
		let formData = FormDataHelper();
		GeneralServices.postRequest(formData, LIST_CATEGORIES).then(
			(resp) => {
				setCategories(resp.data);
				setLoading(false);
			},
			(err) => {
				setLoading(false);
				setError(err);
			},
		);
	}, []);
	useEffect(() => {
		let filteredCategories = [];

		if (showAllCategories) {
			filteredCategories = categories;
		} else if (showApprovedCategories) {
			filteredCategories = categories.filter((category: any) => category.status === 'Y');
		} else if (showPendingCategories) {
			filteredCategories = categories.filter((category: any) => category.status === 'P');
		}

		setFilteredData(filteredCategories);
	}, [categories, showAllCategories, showApprovedCategories, showPendingCategories]);

	return (
		<PageWrapper title={'List Categories'}>
			<Page>
				<Card stretch data-tour='list'>
					<CardHeader>
						<div className='button-container'>
							<div>
								<CardLabel>
									<CardTitle>
										My Projects
										<small className='ms-2'>
											Total:
											{selectTable.values.selectedList.length
												? `${selectTable.values.selectedList.length} / `
												: null}
											{filteredData.length}
										</small>
									</CardTitle>
								</CardLabel>
							</div>
							<div>
								<ButtonGroup>
									<Button
										color='primary'
										isOutline={!showAllCategories}
										onClick={() => {
											setShowAllCategories(true);
											setShowApprovedCategories(false);
											setShowPendingCategories(false);
										}}>
										All Projects
									</Button>
									<Button
										color='success'
										isOutline={!showApprovedCategories}
										onClick={() => {
											setShowAllCategories(false);
											setShowApprovedCategories(true);
											setShowPendingCategories(false);
										}}>
										Completed Projects
									</Button>
									<Button
										color='warning'
										isOutline={!showPendingCategories}
										onClick={() => {
											setShowAllCategories(false);
											setShowApprovedCategories(false);
											setShowPendingCategories(true);
										}}>
										Pending Projects
									</Button>
								</ButtonGroup>
							</div>
						</div>
					</CardHeader>

					<CardBody className='table-responsive' isScrollable>
						<table className='table table-modern table-hover'>
							<thead>
								<tr>
									<th
										scope='col'
										onClick={() => requestSort('id')}
										className='cursor-pointer text-decoration-underline'>
										#
										<Icon
											size='lg'
											className={getClassNamesFor('id')}
											icon='FilterList'
										/>
									</th>
									<th scope='col'>Project Title</th>
									<th scope='col'>Team</th>
									<th scope='col'>Department</th>
									<th scope='col'>From Date</th>
									<th scope='col'>To date</th>

									<th scope='col'>Status</th>
									<th scope='col' className='text-end'>
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								{onCurrentPageItems.length > 0 ? (
									onCurrentPageItems.map((i) => (
										<CategoryTableRow
											key={i.id}
											// eslint-disable-next-line react/jsx-props-no-spreading
											{...i}
											selectName='selectedList'
											selectOnChange={selectTable.handleChange}
											selectChecked={selectTable.values.selectedList.includes(
												// @ts-ignore
												i.id.toString(),
											)}
										/>
									))
								) : (
									<div className='empty-table-message'>
										<DataErrorComponent
											loading={loading}
											error={error}
											length={categories.length}
										/>
									</div>
								)}
							</tbody>
						</table>
					</CardBody>
					<PaginationButtons
						data={items}
						label='items'
						setCurrentPage={setCurrentPage}
						currentPage={currentPage}
						perPage={perPage}
						setPerPage={setPerPage}
					/>
				</Card>
			</Page>
		</PageWrapper>
	);
};
export default ListEmployeeProjects;
