import { AxiosHeaders } from 'axios';
import axios from './axiosConfig';

export const GeneralServices = {
	postRequest,
};

function postRequest(
	formData: any,
	url: string,
	progressEvent: any = null,
	token: any = null,
): Promise<any> {
	return axios({
		method: 'POST',
		url: url,
		data: formData,
		onUploadProgress: progressEvent,
		cancelToken: token,
	})
		.then(function (response: any) {
			if (response.data.statusCode === 200) return response.data;
			else return Promise.reject(response.data.message);
		})
		.catch(function (catchError: any) {
			console.log('GeneralServices', {catchError});
			return Promise.reject(catchError.message || catchError);
		});
}
