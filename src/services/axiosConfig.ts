import axios from 'axios';

// let url = "http://localhost/convier-git-repos/xoticlabs_apis/api/";
// let url = "https://demo.conviersolutions.com/xoticlabs_apis/api/";
// let url = "https://api.xoticlabs.com/api/";
// let url = 'https://api.fandex.com/api/';
let url = 'https://api.medsouk.app/api/'
// let url = 'https://dev.conviersolutions.com/api.medsouk/api/';

const instance = axios.create({
	baseURL: url,
});

export default instance;
