/**
 *  ROUTES URL
 */

export const LIST_ADMINS = 'list_admins';
export const ADMIN_LOGIN = 'admin_login';
export const ADMIN_SIGNUP = 'add_admin';

export const ADD_STORE = 'add_store';
export const LIST_STORES = 'list_stores';
export const EDIT_STORE = 'update_store';

export const ADD_CATEGORY = 'add_category';
export const LIST_CATEGORIES = 'list_categories';
export const EDIT_CATEGORY = 'update_category';
export const UPDATE_CATEGORY_STATUS = 'update_category_status';

export const ADD_PRODUCT = 'add_product';
export const LIST_PRODUCTS = 'list_products';
export const EDIT_PRODUCT = 'update_product';

export const LIST_CUSTOMERS = 'list_customers';

export const LIST_ORDERS = 'list_orders';
export const TOP_STORES_BY_ORDERS = 'top_stores_by_order'


export const LIST_PHARMACIES = 'list_pharmacies';
export const UPDATE_PHARMACY_PROFILE = 'update_pharmacy_profile';
export const UPDATE_PHARMACY_STATUS = 'update_pharmacy_status';

export const TOP_SALES_REPORT = 'top_sales_report';
export const DAILY_SALE_REPORT = 'daily_sale_performance_report';
export const WEEKLY_SALE_REPORT = 'weekly_sale_performance_report';
export const MONTHLY_SALE_REPORT = 'monthly_sale_performance_report';

export const DELETE_IMAGE = 'delete_image';

export const UPDATE_SUPER_ADMIN_PROFILE = 'update_super_admin_profile';
/**
 *  ROUTES URL
 */

// API CONSTANTS
export const API_USERNAME = '43541965248c40cb5adb1761b8302dc5e1107b23';
export const API_PASSWORD = '4c39e469d638981e241e12723b3a529023ade91b';
